import React from 'react';
import ReactDOM from 'react-dom';
import './themes/dist/index.css';
import App from './components/App';
import { BrowserRouter as Router} from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'
import './themes/dist/flexbox.css'
import ScrollToTop from './components/scrollTop'

ReactDOM.render(<Router><ScrollToTop><CookiesProvider><App /></CookiesProvider></ScrollToTop></Router>, document.getElementById('root'));