import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import moment from 'moment-shortformat'
import UserConnexionLoader from './UserConnectionLoader'
import PopUpLogIn from './PopUp/PopUpLogIn'

class WallUserProfile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            posts: null,
            post: "",
            menuConnectionUser: null
        }
        this.postInput = React.createRef()
        this.blocMessages = React.createRef()
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getposts`, { params: { userId: this.props.uploaderId } })
            .then(posts => {
                this.setState({
                    posts: posts.data,
                    loaded: true
                }, () => {
                    this.updateScroll()
                })
            })
    }

    componentDidUpdate() {
        if (this.props.isActive === "timeline") this.updateScroll()
    }

    handleChange = e => {
        this.setState({
            post: e.target.value
        })
    }

    updateScroll() {
        this.blocMessages.current.scrollTop = this.blocMessages.current.scrollHeight
    }

    addPost = e => {
        e.preventDefault()
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/addpost`, { userId: this.props.uploaderId, post: this.state.post }, { withCredentials: true })
                .then(response => {
                    if (response.data.status === 'Post added') {
                        this.setState({
                            post: "",
                            posts: [...this.state.posts, response.data.post]
                        })
                        this.postInput.current.blur()
                        this.updateScroll()
                        window.gtag('event', `wall post`, {
                            'event_category': 'user'
                        })
                    }
                    else if (response.data === 'Post not added') {

                    }
                })
        }
        else {
            this.setState({
                menuConnectionUser: !this.state.menuConnectionUser
            })
        }
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        post: "",        
        menuConnectionUser: false
    })

    toggleMenuConnectionUser = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            menuConnectionUser: !this.state.menuConnectionUser
        })
    }

    render() {
        if (this.state.loaded) {
            const posts = this.state.posts.map(post =>
                <div key={post._id} className="bloc-messages-content">
                    {post.posterId === this.props.userId ?
                        <div className="bloc-message-uploader">
                            <span className="username-wall">{post.posterUsername}</span>
                            <span className="date-wall"> {moment(post.date).short()}</span>
                            <p className="message-wall">{post.post}</p>
                        </div>
                        :
                        <div className="bloc-message-users">
                            <span className="username-wall"><a href={`/user/${post.posterId}`}>{post.posterUsername}</a></span>
                            <span className="date-wall"> {moment(post.date).short()}</span>
                            <p className="message-wall">{post.post}</p>
                        </div>
                    }
                </div>
            )
            return (
                <div className="wall-container">
                    <div className="bloc-messages" ref={this.blocMessages}>
                        {posts}
                    </div>
                    <div>
                        <form onSubmit={this.addPost}>
                            <input className="post-input" ref={this.postInput} type="text" onChange={this.handleChange} value={this.state.post} placeholder="What's on your mind?" required />
                            <button className="send-post-button" type="submit">Send</button>
                        </form>
                    </div>
                    {this.state.menuConnectionUser &&
                        <div className="logInHeader">
                            <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to post" />
                        </div>
                    }
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(WallUserProfile)