//React
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class ContestRules extends Component {

    constructor(props) {
        super(props)
        this.state = {
            listDataFromChild: []
        }
    }

    render() {
        return (
            <div>
                <h3 className="rules-title">Contest rules</h3>
                <div className="rules-container">
                    <p className="rules-items">Your video must be an amateur video</p>
                    <p className="rules-items">Your video must be at least 1mn</p>
                    <p className="rules-items">All submitted videos are subject to approval and may not be accepted if they are found to be non amateur or too bad quality</p>
                    <p>The contest open every Monday at 12am and winners are selected every Sunday at 11:59pm</p>                    
                    <p>We will use your email address to contact you if you win</p>
                    <p>Good Upload!</p>
                </div>
            </div>
        )
    }
}

export default ContestRules