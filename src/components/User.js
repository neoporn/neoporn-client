//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from './UserConnectionLoader'
import VotingHome from './voting/VotingHome'
import WallUserProfile from './WallUserProfile'

class Profile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      profilData: [],
      videos: [],
      totalLength: null,
      redirect: false,
      loaded: false,
      isActive: "videos"
    }
  }

  componentDidMount() {
    this.getData()
    this.getUploaderVideos()
  }

  getData = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getuploaderdata`, { params: { userId: this.props.match.params.id } })
      .then((response) => {
        if (response.data == 'Redirect') {
          this.setState({
            redirect: true
          })
        }
        else {
          this.setState({
            profilData: response.data,
            loaded: true
          }, () => {
            document.title = `${response.data.username}'s profile - neoporn.net`
          })
        }
      })
  }

  getUploaderVideos = e => {
    let { videos } = this.state
    axios.get(`${process.env.REACT_APP_URI}/api/getuploadervideos`, { params: { skipSize: videos.length, userId: this.props.match.params.id } })
      .then((response) => {
        if (response.data) {
          this.setState({
            videos: [...this.state.videos, ...response.data.videos],
            totalLength: response.data.totalLength
          })
        }
      })
  }

  setActive = e => {
    this.setState({
      isActive: e
    })
  }

  moreResults() {
    this.getUploaderVideos()
  }

  render() {
    if (this.state.loaded) {
      const videos = this.state.videos
        .map((data) =>
          <div className="post-my-profile" key={data._id}>
            <Link to={`/video/${data._id}`}>
              <div className="post-home-container-picture">
                <img src={data.urlThumbnail} alt="thumbnail video" />
                <p className="post-home-pseudo no-margin">{data.title}</p>
                <div className="card-content-home flex justify-space-between">
                  <span className="views-home">{data.views} Views</span>
                  <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                </div>
              </div>
            </Link>
          </div>
        )
      return (
        <div>
          {this.state.redirect && <Redirect to='/404' />}
          <h3 className="my-videos-my-profile-title">{this.state.profilData.username}'s profile</h3>
          <button className="recommendations-button"><span onClick={() => this.setActive("videos")} className={`recommendations-span ${this.state.isActive === "videos" ? "active-recommendations" : ""}`}>Videos</span></button>
          <button className="comments-button"><span onClick={() => this.setActive("timeline")} className={`comments-span ${this.state.isActive === "timeline" ? "active-comments" : ""}`}>Timeline</span></button>
          <div className={`${this.state.isActive === "videos" ? "hide-comments" : ""}`}>
            <h4 className="timeline-title">Timeline</h4>
            <WallUserProfile uploaderId={this.state.profilData._id} connected={this.props.connected} isActive={this.state.isActive} />
          </div>
          {this.state.profilData.aboutMe &&
            <div>
              <h4 className="bio-title">About me</h4>
              <div className="bio-container">
                <p className="about-me-content-users">{this.state.profilData.aboutMe}</p>
              </div>
            </div>
          }
          {this.state.profilData.links &&
            <div>
              <h4 className="bio-title">Links</h4>
              <div className="links-container">
                <div className="links-content">
                  {this.state.profilData.links.website &&
                    <div className="links-link">
                      <span className="website-link-icon">www</span>
                      <span className="my-link"> {this.state.profilData.links.website}</span>
                    </div>
                  }
                  {this.state.profilData.links.facebook &&
                    <div className="links-link">
                      <i className="fab fa-facebook-f"></i>
                      <span className="my-link"> {this.state.profilData.links.facebook}</span>
                    </div>
                  }
                  {this.state.profilData.links.twitter &&
                    <div className="links-link">
                      <i className="fab fa-twitter"></i>
                      <span className="my-link"> {this.state.profilData.links.twitter}</span>
                    </div>
                  }
                  {this.state.profilData.links.instagram &&
                    <div className="links-link">
                      <i className="fab fa-instagram"></i>
                      <span className="my-link"> {this.state.profilData.links.instagram}</span>
                    </div>
                  }
                </div>
              </div>
            </div>
          }
          <div className="cards-home-container-my-profile">
            <div className="cards-content-my-profile">
              {videos.length > 0 ?
                <div className={this.state.isActive === "timeline" ? "hide-recommendations" : ""}>
                  <h4 className="videos-title">Videos</h4>
                  {videos}
                  {this.state.videos.length < this.state.totalLength &&
                    <div className="more-button-home-container">
                      <button onClick={() => this.moreResults()} className="more-button-home">More</button>
                    </div>
                  }
                </div>
                :
                <p className="no-videos-yet">No videos yet</p>
              }
            </div>
          </div>
        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(withRouter(Profile))