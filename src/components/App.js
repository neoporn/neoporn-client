import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../themes/dist/App.css'
import Header from './header'
import Footer from './footer'
import MyRouter from './myRouter'
import PopUpLogIn from './PopUp/PopUpLogIn'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      showLogin: false
    }
  }

  toggleLogIn = () => {
    this.setState({ showLogin: !this.state.showLogin })
  }

  closePopUpLogIn = () => {
    this.setState({ showLogin: !this.state.showLogin })
  }
  
  render() {
    return (
      <div className="App">
        <Header />
        <div className="content">
          <MyRouter />
          {this.state.showLogin && <PopUpLogIn closePopUpLogIn={this.closePopUpLogIn} />}
        </div>
        <Footer />
      </div>
    )
  }
}

export default App