//React
import React, { Component } from 'react';
//Router
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';

class Footer extends Component {
  render () {
    return (
      <footer className="footer">
        <p>
          <Link className="link-footer" to="/contact">Contact</Link>-
          <a className="link-footer" href="#" target="_blank">Terms and conditions</a>
        </p>
        <p>Copyright (c) 2018 - Neoporn - All Rights Reserved.</p>
      </footer>
    )
  }
}

export default Footer;