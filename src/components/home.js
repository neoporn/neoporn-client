//React
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
//Components
import CardHome from './CardHome'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'

class Home extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            listDataFromChild: []
        }
    }

    componentDidMount() {
        document.title = "Neoporn free amateur videos - neoporn.net"
    }

    searchFrom = dataFromChild => {
        this.setState({ listDataFromChild: dataFromChild })
    }

    render() {
        return (
            <div>
                <div className="cards-home-container">
                    <h1 className="title-home-page">Videos 100% amateurs!</h1>
                    <CardHome searchFormProps={this.state.listDataFromChild} searchForm={this.searchFrom} />
                </div>
            </div>
        )
    }
}

export default withCookies(Home)