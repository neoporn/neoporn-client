//React
import React from 'react'
//Router
import { Redirect } from 'react-router-dom'
import axios from 'axios'
//Components
import userConnexionLoader from './UserConnectionLoader'
import Resumable from 'resumablejs'
import TagsInput from 'react-tagsinput'

class Upload extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            containerOn: this.props.showModify,
            formData: {
                title: "",
                description: "",
                tags: [],
                tag: ""
            },
            message: "",
            r: "",
            progressBar: false,
            fileName: null
        }
    }

    handleChangeTags = (tags) => {
        this.setState(state => ({
            formData: {
                ...this.state.formData,
                tags: tags
            }
        }))
    }

    handleChange = e => {
        const { value, name } = e.target
        const { formData } = this.state
        this.setState({
            formData: {
                ...formData,
                [name]: value
            }
        })
    }

    componentDidMount() {
        window.gtag('event', `upload button`, {
            'event_category': 'user',
            'event_label': `Connected user`
        })
        document.title = `Upload - neoporn.net`
        let button = document.getElementsByClassName("add-video")
        var elem = document.getElementById("myBar")

        let r = new Resumable({
            target: `${process.env.REACT_APP_TRANSCODING_SERVER_URI}/api/uploadvideo`,
            chunkSize: 1 * 1024 * 1024,
            maxFiles: 1,
            simultaneousUploads: 5,
            fileType: ['mp4', 'avi', 'ogg', 'mov', 'wave']
        })
        r.assignBrowse(button)
        this.setState({
            r: r
        }, () => {
            this.state.r.on('progress', () => {
                this.setState({
                    message: "",
                    progressBar: true
                }, () => {
                    var width = this.state.r.progress() * 100
                    document.getElementById("myBar").style.width = width + '%'
                })
            })
            this.state.r.on('fileAdded', function (file, event) {
                this.setState({
                    fileName: file.fileName
                })
            }.bind(this))
            this.state.r.on('fileSuccess', function (file, event) {
                axios.post(`${process.env.REACT_APP_URI}/api/postvideo`, { fields: this.state.formData, file: file.file.uniqueIdentifier }, { withCredentials: true })
                    .then(() => {
                        console.log("Success")
                    })
                this.setState({
                    progressBar: false,
                    fileName: null,
                    formData: {
                        ...this.state.formData,
                        title: "",
                        description: "",
                        tags: []
                    },
                    message: `Uploaded, your video should appear once approuved. \nIf it takes more than 24h please reupload your video.`
                })
            }.bind(this))
        })
    }

    upload = e => {
        e.preventDefault()
        if (this.state.formData.title && this.state.fileName) {
            this.state.r.upload()
        }
        else {
            this.setState({
                message: "Please fill all the fields"
            })
        }
    }

    submit = e => {

    }

    render() {
        if (!this.props.connected) return <Redirect to="/nomatch" />
        return (
            <div className="upload-form">
                <form onSubmit={e => this.upload(e)}>
                    <label htmlFor="title">Title</label>
                    <br />
                    <input type="text" name="title" id="title" className="title-upload" value={this.state.formData.title} onChange={this.handleChange} required />
                    <br />
                    <label htmlFor="description">Description</label>
                    <br />
                    <input type="text" name="description" id="description" className="description-upload" value={this.state.formData.description} onChange={this.handleChange} />
                    <br />
                    <label htmlFor="video">Video</label>
                    <br />
                    <button type="button" className="add-video">Add video</button>
                    {this.state.progressBar &&
                        <div id="myProgress">
                            <div id="myBar"></div>
                        </div>
                    }
                    <div id="results" className="results">{this.state.fileName}</div>
                    <label htmlFor="tags">Tags</label>
                    <br />
                    <div className="tags-input-container flex justify-space-between">
                        <TagsInput
                            ref="tagsinput"
                            value={this.state.formData.tags}
                            onChange={this.handleChangeTags}
                            onlyUnique={true}
                            maxTags="16"
                            removeKeys={[]} />
                        <button type="button" id="input" className="tags-input-button" onClick={() => this.refs.tagsinput.accept()}>Add tag</button>
                    </div>
                    <button type="submit" className="upload-button">Upload</button>
                    <br />
                </form>
                <div className="upload-message">{this.state.message}</div>
            </div>
        )
    }
}

export default userConnexionLoader(Upload)