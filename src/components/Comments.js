import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import moment from 'moment-shortformat'
import PopUpLogIn from './PopUp/PopUpLogIn'

class Comments extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            comments: null,
            comment: "",
            menuConnectionUser: null
        }
        this.commentInput = React.createRef()
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getcomments`, { params: { videoId: this.props.videoId } })
            .then(comments => {
                this.setState({
                    comments: comments.data,
                    loaded: true
                })
            })
    }

    handleChange = e => {
        this.setState({
            comment: e.target.value
        })
    }

    addComment = e => {
        e.preventDefault()
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/addcomment`, { videoId: this.props.videoId, comment: this.state.comment }, { withCredentials: true })
                .then(response => {
                    if (response.data.status === 'Comment added') {
                        this.setState({
                            comment: "",
                            comments: [response.data.comment, ...this.state.comments]
                        })
                        this.commentInput.current.blur()
                    }
                    else if (response.data === 'Comment not added') {

                    }
                })
        }
        else {
            this.setState({
                comment: "",
                menuConnectionUser: !this.state.menuConnectionUser
            })
        }
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        menuConnectionUser: false
    })

    toggleMenuConnectionUser = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            menuConnectionUser: !this.state.menuConnectionUser
        })
    }

    render() {
        if (this.state.loaded) {
            const comments = this.state.comments.map(comment =>
                <div key={comment._id} className="comments-content">
                    <span className="username-comment"><Link className="username-comment-link" to={`/user/${comment.userId}`}>{comment.userUsername}</Link></span>
                    <span className="time-ago-comment">{moment(comment.date).short()}</span>
                    <p className="comment-comment">{comment.comment}</p>
                </div>
            )
            return (
                <div>
                    <form onSubmit={this.addComment}>
                        <input ref={this.commentInput} type="text" className="add-comment-input" placeholder="Add a comment" onChange={this.handleChange} value={this.state.comment} />
                        {this.state.comment.length >= 1 && <button className="add-comment-button">Comment</button>}
                        {this.state.menuConnectionUser &&
                            <div className="logInHeader">
                                <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to comment" />
                            </div>
                        }
                    </form>
                    <div className="comments-container">
                        {comments}
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default Comments