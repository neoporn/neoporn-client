import React from 'react'
//Components
import UserConnexionLoader from './UserConnectionLoader'
import { withRouter, Redirect, Link } from 'react-router-dom'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'
import classNames from 'classnames'
import moment from 'moment-shortformat'

class Notifications extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            notifications: [],
            notificationsData: [],
            notificationNumber: 0,
            toggleDropdown: false,
            notificationsVideo: null,
            notificationsNewVideo: null,
            loaded: false
        }
    }

    componentDidMount() {
        this.getNotifications()
    }

    getNotifications() {
        axios.get(`${process.env.REACT_APP_URI}/api/getnotifications`, { withCredentials: true })
            .then(response => {
                const { notifications } = response.data
                const notificationsNumber = notifications.filter(notificationNumber => notificationNumber.seen == false)
                const notifNewVideoMap = notifications.map(notif => {
                    const videos = response.data.videos.filter(video => {
                        if (notif.notifType === "verificationVideo") {
                            return video._id == notif.verificationVideo.videoId
                        }
                    })
                    let uploader
                    if (notif.notifType === "followedNewVideo") {
                        const newUploader = response.data.uploaderNewVideo.filter(uploader => uploader._id == notif.followedNewVideo.uploaderId)
                        uploader = newUploader.map(data => data)
                    }
                    return { ...notif, uploader, videos }
                })
                this.setState({
                    notifications: notifications,
                    notificationNumber: notificationsNumber,
                    notificationsVideo: response.data.videos,
                    notificationsNewVideo: notifNewVideoMap,
                    loaded: true
                })
            })
    }

    notificationsDropdown = e => {
        e.stopPropagation()
        e.preventDefault()
        this.setState({
            toggleDropdown: !this.state.toggleDropdown
        })
    }

    handleClickOutside = e => {
        this.setState({
            toggleDropdown: false
        })
    }

    notificationViewed = e => {
        axios.put(`${process.env.REACT_APP_URI}/api/notificationViewed`, null, { withCredentials: true })
            .then(response => {
                if (response.data === "Notif viewed") {
                    this.setState({
                        notificationNumber: 0
                    })
                }
            })
        this.handleClickOutside()
    }

    render() {
        if (this.state.loaded) {
            const notificationsNewVideo = this.state.notificationsNewVideo.map(items => {
                return (
                    <div key={items._id}>
                        <div className="notification">
                            <div className="notif-content">
                                {items.notifType === "followedNewVideo" &&
                                    <div>
                                        {items.uploader.map(uploader => <span key={uploader._id}><a href={`/user/${uploader._id}`}>{uploader.username}</a> </span>)}
                                        <span>uploaded a video </span>
                                        {items.seen}
                                        <br />
                                        <p className="notifs-time-ago">{moment(items.date).short()}</p>
                                    </div>
                                }
                                {items.notifType === "verificationVideo" &&
                                    <div>
                                        {items.videos.map(video => <span key={video._id}>Your video: "{items.verificationVideo.stateVerification ? <a className="notifications-as" href={`/video/${video._id}`}>{video.title}</a> : video.title}" </span>)}
                                        {items.verificationVideo.stateVerification ? <span>is now online </span> : <span>has been rejected for reason: {items.verificationVideo.reason}</span>}
                                        {items.seen}
                                        <br />
                                        <p className="notifs-time-ago">{moment(items.date).short()}</p>
                                    </div>
                                }
                            </div>
                        </div>
                    </div >
                )
            }).reverse()
            return <span>
                <span>
                    <span onClick={this.notificationsDropdown} className="notifications-bubble">
                        <i className="fas fa-bell notifications-icon" onClick={this.notificationViewed}></i>
                        {this.state.notificationNumber.length > 0 && <span className="notification-number">{this.state.notificationNumber.length}</span>}
                    </span>
                    {this.state.toggleDropdown && <div className="container-notifications-bubble">
                        {this.state.notifications.length > 0 ?
                            <ul className="list-notifs-menu">
                                <li>{notificationsNewVideo}</li>
                            </ul>
                            :
                            <p className="text-center">No notifications yet</p>
                        }
                    </div>}
                </span>
            </span>
        }
        else return null
    }
}

export default UserConnexionLoader(onClickOutside(Notifications))