import { decorate, observable, configure, action, reaction } from 'mobx'
configure({enforceActions: true})

class SearchBarStore {

    searchString = ""

    addString(e) {
        this.searchString = e
      }
}

decorate(SearchBarStore, {
    searchString: observable,
    addString: action
})

const NewSearchBarStore = new SearchBarStore()


const reaction1 = reaction(
    () => NewSearchBarStore.searchString,
    (searchString, reaction) => {
        
    }
);

export default NewSearchBarStore