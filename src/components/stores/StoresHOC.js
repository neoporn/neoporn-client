//React
import React from 'react'
//Components
import SearchBarStore from "./SearchBarStore"
import { observer } from "mobx-react"

const storesHOC = (WrappedComponent) => {
    class storesHOCC extends React.Component {

        constructor(props) {
            super(props)
            this.state = {
                searchString: ""
            }
        }

        render() {
          const { searchString } = this.state
            return (
                <WrappedComponent {...this.state} {...this.props} {...SearchBarStore} />
            )
        }
    }
    return observer(storesHOCC)
}

export default storesHOC