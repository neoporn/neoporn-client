import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Views from '../Views'
import UserConnectionLoader from '../UserConnectionLoader'
import PopUpLogIn from '../PopUp/PopUpLogIn'
import onClickOutside from 'react-onclickoutside'

class VotingVideo extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            views: this.props.views,
            likes: this.props.likes,
            dislikes: this.props.dislikes,
            likesData: null,
            dislikesData: null,
            voteMessage: null,
            menuConnectionUser: null,
            activeLike: null,
            activeDislike: null,
            onMessage: null,
            favorites: null
        }
        this.video = React.createRef()
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getvotes`, { params: { videoId: this.props.videoId } })
            .then(votes => {
                this.setState({
                    loaded: true,
                    likesData: votes.data.likes,
                    dislikesData: votes.data.dislikes
                })
                const likes = this.state.likesData.filter(data => {
                    return data.userId === this.props.userId
                })
                    .map(data => {
                        return data.userId
                    })
                const dislikes = this.state.dislikesData.filter(data => {
                    return data.userId === this.props.userId
                })
                    .map(data => {
                        return data.userId
                    })
                if (likes.length > 0 && dislikes.length === 0) {
                    this.setState({
                        activeThumb: "likes"
                    })
                }
                else if (dislikes.length > 0 && likes.length === 0) {
                    this.setState({
                        activeThumb: "dislikes"
                    })
                }
            })
        this.getMyFavorites()
    }

    componentWillUnmount() {
        clearTimeout(this.messageTimeout)
    }

    vote = (e, event) => {
        const name = event.target.getAttribute("name")
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/vote`, { videoId: this.props.videoId, vote: name }, { withCredentials: true })
                .then(response => {
                    if (response.data === 'Added') {
                        this.setState({
                            [name]: this.state[name] + 1,
                            activeThumb: name
                        })
                        window.gtag('event', `video ${name}`, {
                            'event_category': 'user',
                            'event_label': `${this.props.videoName}`
                        })
                    }
                    else if (response.data === 'Removed') {
                        this.setState({
                            [name]: this.state[name] - 1,
                            activeThumb: null
                        })
                        window.gtag('event', `video ${name} removed`, {
                            'event_category': 'user',
                            'event_label': `${this.props.videoName}`
                        })
                    }
                    else if (response.data === 'AL RD') {
                        this.setState({
                            [name]: this.state[name] + 1,
                            dislikes: this.state.dislikes - 1,
                            activeThumb: name
                        })
                        window.gtag('event', `video ${name} AL RD`, {
                            'event_category': 'user',
                            'event_label': `${this.props.videoName}`
                        })
                    }
                    else if (response.data === 'AD RL') {
                        this.setState({
                            [name]: this.state[name] + 1,
                            likes: this.state.likes - 1,
                            activeThumb: name
                        })
                        window.gtag('event', `video ${name} AD RL`, {
                            'event_category': 'user',
                            'event_label': `${this.props.videoName}`
                        })
                    }
                })
        }
        else {
            this.setState({
                menuConnectionUser: !this.state.menuConnectionUser
            })
        }
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        menuConnectionUser: false
    })

    toggleMenuConnectionUser = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            menuConnectionUser: !this.state.menuConnectionUser
        })
    }

    getMyFavorites = e => {
        if (this.props.connected) {
            axios.get(`${process.env.REACT_APP_URI}/api/getmyfavorites`, { params: { videoId: this.props.videoId }, withCredentials: true })
                .then((response) => {
                    if (response.data) {
                        this.setState({
                            favorites: response.data.favorite
                        })
                    }
                })
        }
    }

    addFavorites = (e) => {
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/addfavorites`, { videoId: this.props.videoId }, { withCredentials: true })
                .then((response) => {
                    if (response.data) {
                        this.setState({
                            favorites: !this.state.favorites
                        })
                        if (response.data === "Favorite added") {
                            window.gtag('event', `favorite added`, {
                                'event_category': 'user',
                                'event_label': `${this.props.videoName}`
                            })
                        }
                        else {
                            window.gtag('event', `favorite removed`, {
                                'event_category': 'user',
                                'event_label': `${this.props.videoName}`
                            })
                        }
                    }
                })
        }
        else {
            this.setState({
                menuConnectionUser: !this.state.menuConnectionUser
            })
        }
    }

    render() {
        if (this.state.loaded) {
            return (
                <div className="thumbs-container flex justify-space-between">
                    <div className="views-video-left">
                        <span>{this.state.views} Views</span>
                    </div>
                    <div>
                        <div onClick={this.addFavorites} className="add-favorites"><i className={`fas fa-heart ${this.state.favorites ? "active-favorite" : "inactive-favorite"}`}></i></div>
                    </div>
                    <div className="stats-video-container">
                        <div className="likes-dislikes-video">
                            <i onClick={(e) => this.vote('like', e)} name="likes" className={"fas fa-thumbs-up thumbs-vote-up " + (this.state.activeThumb === 'likes' ? 'active-thumb' : '')}></i>
                            <span> {this.state.likes}</span>
                            <i onClick={(e) => this.vote('dislike', e)} name="dislikes" className={"fas fa-thumbs-down thumbs-vote-down " + (this.state.activeThumb === 'dislikes' ? 'active-thumb' : '')}></i>
                            <span> {this.state.dislikes}</span>
                        </div>
                        {this.state.menuConnectionUser &&
                            <div className="logInHeader">
                                <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to vote" />
                            </div>
                        }
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnectionLoader(VotingVideo)