import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class VotingRecommendedVideos extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            likes: this.props.likes,
            dislikes: this.props.dislikes,
            percentageVotes: null
        }
        this.video = React.createRef()
    }

    componentDidMount() {
        this.setPercentageVotes()
    }

    setPercentageVotes = () => {
        const { likes } = this.state
        const { dislikes } = this.state
        const percentageVotes = Math.round(likes / (likes + dislikes) * 100) || 0

        this.setState({
            percentageVotes: percentageVotes
        })
    }

    render() {
        return (
            <div className="thumbs-container">
                <i className="fas fa-thumbs-up recommended-videos-thumbs-up"></i>
                <span> {this.state.percentageVotes} % </span>
            </div>
        )
    }
}

export default VotingRecommendedVideos