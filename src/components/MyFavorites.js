//React
import React, { Component } from 'react'
//Router
import { Link, Redirect } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from './PopUp/PopUpModifyMyProfile'
import PopUpPostItem from './PopUp/PopUpPostItem'
import UserConnexionLoader from './UserConnectionLoader'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'
import VotingHome from './voting/VotingHome'
import WallUserProfile from './WallUserProfile'

class MyFavorites extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newItem: null,
            showModify: false,
            profilData: [],
            picture: null,
            videos: [],
            totalLength: null,
            messages: {
                messageProfilUpdated: ""
            },
            isLoaded: false,
            followers: null,
            following: null,
            isActive: "videos",
            bioExpanded: false,
            aboutMe: ""
        }
    }

    componentDidMount() {
        document.title = "My profile - neoporn.net"
        this.getData()
        this.getMyVideos()
        this.getMyFollowers()
    }

    toggleModify = (content) => {
        this.setState({ showModify: content })
    }

    closePopUpModify = () => {
        this.setState({ showModify: !this.state.showModify })
    }

    getData = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getprofildata`, { params: { userId: this.props.userId } })
            .then((response) => {
                this.setState({
                    profilData: response.data
                })
            })
    }

    getMyVideos = e => {
        let { videos } = this.state
        axios.get(`${process.env.REACT_APP_URI}/api/getmyfavoritesvideos`, { params: { skipSize: videos.length, userId: this.props.userId } })
            .then((response) => {
                if (response.data) {
                    this.setState({
                        videos: [...this.state.videos, ...response.data.videos],
                        totalLength: response.data.totalLength
                    })
                }
            })
    }

    getMyFollowers = e => {
        axios.get(`${process.env.REACT_APP_URI}/api/getmyfollowers`, { withCredentials: true })
            .then((response) => {
                if (response.data) {
                    this.setState({
                        followers: response.data.followers,
                        following: response.data.following,
                        isLoaded: true
                    })
                }
            })
    }

    setStateMessages = e => {
        this.setState({
            messages: {
                messageProfilUpdated: e
            }
        })
        this.getData()
    }

    newItem = newItem => {
        this.myItemsToSell.getMyItemsToSell(newItem)
    }

    handleOutsideClick = () => {
        this.setState({
            openedGallery: !this.state.openedGallery
        })
    }

    setActive = e => {
        this.setState({
            isActive: e
        })
    }

    handleChange = e => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    submitBio = e => {
        e.preventDefault()
        axios.post(`${process.env.REACT_APP_URI}/api/addbio`, { aboutMe: this.state.aboutMe }, { withCredentials: true })
            .then((response) => {
                if (response.data === "Ok") {
                    this.setState(prevState => ({
                        profilData: {
                            ...prevState.profilData,
                            aboutMe: this.state.aboutMe
                        },
                        bioExpanded: false,
                        aboutMe: ""
                    }))
                    window.gtag('event', `about me`, {
                        'event_category': 'user'
                    })
                }
            })
    }

    expandAddBio = e => {
        this.setState({
            bioExpanded: !this.state.bioExpanded
        })
    }

    moreResults() {
        this.getMyVideos()
    }

    render() {
        if (!this.props.connected) this.props.history.push("/")
        if (this.state.isLoaded) {
            const videos = this.state.videos
                .map((data) =>
                    <div className="post-my-profile" key={data._id}>
                        <Link to={`video/${data._id}`}>
                            <div className="post-home-container-picture">
                                <img src={data.urlThumbnail} alt="thumbnail video" />
                                <p className="post-home-pseudo no-margin">{data.title}</p>
                                <div className="card-content-home flex justify-space-between">
                                    <span className="views-home">{data.views} Views</span>
                                    <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                                </div>
                            </div>
                        </Link>
                    </div>
                )
            const followers = this.state.followers
                .map((data) =>
                    <li key={data._id} className="followers-list-content">
                        <Link to={`user/${data._id}`}><span className="followers-my-profile">{data.username}</span></Link>
                    </li>
                )
            const following = this.state.following
                .map((data) =>
                    <li key={data._id} className="following-list-content">
                        <Link to={`user/${data._id}`}><span className="following-my-profile">{data.username}</span></Link>
                    </li>
                )
            return (
                <div>
                    <h2 className="my-videos-my-profile-title">{this.state.profilData.username}</h2>
                    <div className="cards-home-container-my-profile flex justify-space-between">
                        <div className="cards-content-my-favorites">
                            <h3 className="my-videos-my-profile">My favorites</h3>
                            {videos.length > 0 ?
                                <div className={this.state.isActive === "timeline" ? "hide-recommendations" : ""}>
                                    <h4 className="videos-title">Videos</h4>
                                    {videos}
                                    {this.state.videos.length < this.state.totalLength &&
                                        <div className="more-button-home-container">
                                            <button onClick={() => this.moreResults()} className="more-button-home">More</button>
                                        </div>
                                    }
                                </div>
                                :
                                <p className="no-videos-yet">No videos yet</p>
                            }
                        </div>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(onClickOutside(MyFavorites))