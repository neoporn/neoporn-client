import React from 'react'
import socketIOClient from 'socket.io-client'
import axios from 'axios'

class Contact extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      contactForm: {
        email: "",
        subject: "",
        message: ""
      }
    }
  }

  componentDidMount() {
    document.title = 'Contact - neoporn.net'
  }

  handleChange = e => {
    const { value, name } = e.target
    const { contactForm } = this.state
    this.setState({
      contactForm: {
        ...contactForm,
        [name]: value
      }
    })
  }

  contactForm = e => {
    e.preventDefault()
    axios.post(`${process.env.REACT_APP_URI}/api/contact`, this.state.contactForm, {
      withCredentials: true
    })
    .then(data => {
      if (data.data === 'Message sent') {
        this.setState({
          message: 'Got your message! We will reply you as soon as possible'
        })
      }
    })
  }

  render() {
    return (
      <div>
        <h2 className="title-contact">A question, a bug, an advice?</h2>
        <form className="form-contact form-group" onSubmit={this.contactForm}>
          <input type="text" className="form-control" id="email" placeholder="Email" name="email" value={this.state.contactForm.email} onChange={this.handleChange} required />
          <input type="text" className="form-control" id="sujet" placeholder="Sujet" name="subject" value={this.state.contactForm.subject} onChange={this.handleChange}  required />
          <textarea id="message" className="form-control" cols="30" rows="10" placeholder="Message" name="message" value={this.state.contactForm.message} onChange={this.handleChange}  required></textarea>
          <button className="contact-button" type="submit">Send</button>
        </form>
        {this.state.message}
      </div>
    )
  }
}

export default Contact