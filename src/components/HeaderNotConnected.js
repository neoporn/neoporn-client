//React
import React from 'react'
//Router
import { withRouter, Link } from 'react-router-dom'

import PopUpLogIn from './PopUp/PopUpLogIn'
import onClickOutside from 'react-onclickoutside'
import SearchBar from './SearchBar'
import SearchBarStore from "./stores/SearchBarStore"
import ActionLink from './ActionLink'

//Components
import PopUpHeaderMenuConnectedUser from './PopUp/PopUpHeaderMenuConnectedUser'

class HeaderNotConnected extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      menuConnectionUser: false,
      showMobileMenu: false,
      showMobileSearch: false
    }
  }

  notConnectedMessage = e => {
    this.setState({
      notConnectedMessage: true
    })
    window.gtag('event', `upload button`, {
      'event_category': 'user',
      'event_label': `Not connected user`
    })
  }

  home = e => {
    SearchBarStore.addString("")
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    menuConnectionUser: false,
    notConnectedMessage: false
  })

  toggleMenuConnectionUser = (e) => {
    e.stopPropagation()
    e.preventDefault()

    this.setState({
      menuConnectionUser: !this.state.menuConnectionUser,
      showMobileMenu: false
    })
  }

  showMobileMenu = e => {
    this.setState({
      showMobileMenu: !this.state.showMobileMenu,
      showMobileSearch: false
    })
  }

  showMobileSearch = e => {
    this.setState({
      showMobileSearch: !this.state.showMobileSearch,
      showMobileMenu: false
    })
  }

  render() {
    const { handleOutsideClick } = this.props
    return (
      <div>
        <div className="flex align-center justify-space-between">
          <div onClick={this.showMobileMenu} className="mobile-menu-icon"><i className="fas fa-bars"></i></div>
          <div className="header-title"><a onClick={this.home} className="link-header" href="/">Neo<span className="porn">porn</span></a></div>
          <div onClick={this.showMobileSearch} className="mobile-search-icon"><i className="fas fa-search"></i></div>
          <div className="containerLogInHeader flex justify-flex-end align-center">
            <SearchBar />
            <span className="no-break-line-flex">
              <div className="logInHeader">
                <span className="upload-icon-container" onClick={this.notConnectedMessage}><span className="upload-header"><i className="fas fa-cloud-upload-alt upload-icon"></i> Upload</span></span>
                <a href="#" onClick={this.toggleMenuConnectionUser} className="toggle">Sign in</a>
              </div>
            </span>
          </div>
        </div>
        <div className="mobile-search">

          {this.state.showMobileSearch ?
            <div>
              <SearchBar toggleSearchBar={this.showMobileSearch} />
            </div>
            :
            this.state.showMobileMenu &&
            <div>
              <ul className="ListItemsMenuConnectedUser sign-in-link-header-container">
                <li><a href="#" onClick={this.toggleMenuConnectionUser} className="toggle sign-in-link-header">Sign in</a></li>
              </ul>
            </div>
          }
        </div>
        {this.state.menuConnectionUser ?
          <PopUpLogIn handleClickOutside={this.handleClickOutside} />
          :
          this.state.notConnectedMessage &&
          <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to upload" />
        }
      </div>
    )
  }
}

export default onClickOutside(HeaderNotConnected)