import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import _ from 'lodash'
import VotingHome from './voting/VotingHome'

class Tag extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            totalLength: null,
            cards: [],
            sortBy: null,
            searchString: null,
            idAlreadyGot: [],
            tagTitle: null,
            notFoundMessage: false
        }
    }

    componentDidUpdate(prevProps, nextProps) {
        if (prevProps != this.props) {
            this.getData()
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = e => {
        let { cards } = this.state
        let { idAlreadyGot } = this.state

        axios.get(`${process.env.REACT_APP_URI}/api/getsearchresult`, { params: { searchString: this.props.match.params.tag, skipSize: cards.length } })
            .then(async response => {
                if (response.data.videos) {
                    let idAlreadyGot = await response.data.videos.map(data => data._id)
                    this.setState({
                        cards: response.data.videos,
                        totalLength: response.data.totalLength,
                        loaded: true,
                        idAlreadyGot: idAlreadyGot,
                        tagTitle: this.props.match.params.tag,
                        notFoundMessage: false
                    })
                }
                else {
                    this.setState({
                        notFoundMessage: true,
                        loaded: true
                    })
                }
            })
    }

    getSearchResult = e => {
        let { cards } = this.state
        let { idAlreadyGot } = this.state
        axios.get(`${process.env.REACT_APP_URI}/api/getsearchresult`, { params: { skipSize: cards.length, searchString: this.props.match.params.tag, idAlreadyGot: idAlreadyGot } })
            .then(async response => {
                if (response.data.videos) {
                    let idAlreadyGot = await response.data.videos.map(data => data._id)
                    this.setState({
                        cards: [...cards, ...response.data.videos],
                        totalLength: response.data.totalLength,
                        loaded: true,
                        idAlreadyGot: [...this.state.idAlreadyGot, ...idAlreadyGot],
                        notFoundMessage: false
                    })
                }
            })
    }

    moreResults() {
        this.getSearchResult()
    }

    votes = e => {
        this.setState({
            likes: e.likes,
            dislikes: e.dislikes
        })
    }

    sortBy = e => {
        this.setState({
            sortBy: e
        })
    }

    render() {
        if (this.state.loaded) {
            const cards = this.state.cards
                .map((data) =>
                    <div className="post-home" key={data._id}>
                        <Link to={`/video/${data._id}`}>
                            <div className="post-home-container-picture">
                                <img src={data.urlThumbnail} alt="thumbnail video" />
                                <p className="post-home-pseudo no-margin">{data.title}</p>
                                <div className="card-content-home flex justify-space-between">
                                    <span className="views-home">{data.views} Views</span>
                                    <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                                </div>
                            </div>
                        </Link>
                    </div>
                )

            const sortByRating = _.orderBy(this.state.cards, [function (a) {
                return a.likes
            }], ['desc'])
                .map((data) =>
                    <div className="post-home" key={data._id}>
                        <Link to={`/video/${data._id}`}>
                            <div className="post-home-container-picture">
                                <img src={data.urlThumbnail} alt="thumbnail video" />
                                <p className="post-home-pseudo no-margin">{data.title}</p>
                                <div className="card-content-home flex justify-space-between">
                                    <span className="views-home">{data.views} Views</span>
                                    <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                                </div>
                            </div>
                        </Link>
                    </div>
                )

            const sortByDate = _.orderBy(this.state.cards, ['date'], ['desc'])
                .map((data) =>
                    <div className="post-home" key={data._id}>
                        <Link to={`/video/${data._id}`}>
                            <div className="post-home-container-picture">
                                <img src={data.urlThumbnail} alt="thumbnail video" />
                                <p className="post-home-pseudo no-margin">{data.title}</p>
                                <div className="card-content-home flex justify-space-between">
                                    <span className="views-home">{data.views} Views</span>
                                    <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                                </div>
                            </div>
                        </Link>
                    </div>
                )

            return <div className="cards-home-container">
                {this.state.notFoundMessage ?
                    <div className="not-found-message-tag-container">
                        <div className="not-found-message-tag">
                            <p>
                                Oops!
                                <br />
                                No videos for this query yet
                                <br />
                                You can upload your own videos or try again later
                                <br />
                            </p>
                        </div>
                        <div className="not-found-message-tag-suggestions">
                            <span>Suggestion</span>
                            <p>
                                Try more general keywords
                                <br />
                                Try different keywords that means the same thing
                                </p>
                        </div>
                    </div>
                    :
                    <div>
                        <h1 className="tag-title">{this.state.tagTitle}</h1>
                        <div className="sort-by">
                            <span className={"by-date" + (this.state.sortBy === 'date' ? ' active-sort' : '')} name="date" onClick={() => this.sortBy('date')}>By date</span>
                            <span className={"by-rating" + (this.state.sortBy === 'rating' ? ' active-sort' : '')} name="rating" onClick={() => this.sortBy('rating')}>By rating</span>
                        </div>
                        <div className="cards-content">
                            {this.state.sortBy === 'date' && sortByDate}
                            {this.state.sortBy === 'rating' && sortByRating}
                            {this.state.sortBy === null && cards}
                            {this.state.cards.length < this.state.totalLength &&
                                <div className="more-button-home-container">
                                    <button onClick={() => this.moreResults()} className="more-button-home">More</button>
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>
        }
        else return null
    }
}

export default Tag