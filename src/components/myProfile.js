//React
import React, { Component } from 'react'
//Router
import { Link, Redirect } from 'react-router-dom'
//Components
import PopUpModifyMyProfile from './PopUp/PopUpModifyMyProfile'
import PopUpPostItem from './PopUp/PopUpPostItem'
import UserConnexionLoader from './UserConnectionLoader'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'
import VotingHome from './voting/VotingHome'
import WallUserProfile from './WallUserProfile'
import { truncate } from 'fs';

class MyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      newItem: null,
      showModify: false,
      profilData: [],
      picture: null,
      videos: [],
      totalLength: null,
      messages: {
        messageProfilUpdated: ""
      },
      isLoaded: false,
      followers: null,
      following: null,
      isActive: "videos",
      bioExpanded: false,
      linksExpanded: false,
      aboutMe: "",
      links: {
        websiteLink: "",
        facebookLink: "",
        twitterLink: "",
        instagramLink: ""
      }
    }
  }

  componentDidMount() {
    document.title = "My profile - neoporn.net"
    if (this.props.connected) this.getAllData()
  }

  toggleModify = (content) => {
    this.setState({ showModify: content })
  }

  closePopUpModify = () => {
    this.setState({ showModify: !this.state.showModify })
  }

  getAllData = async () => {
    const getData = await this.getData()
    const getMyVideos = await this.getMyVideos()
    const getMyFollowers = await this.getMyFollowers()
    this.setState({
      isLoaded: true
    })
  }

  getData = e => {
    return new Promise((resolve, reject) => {
      axios.get(`${process.env.REACT_APP_URI}/api/getprofildata`, { params: { userId: this.props.userId } })
        .then((response) => {
          this.setState({
            profilData: response.data
          }, () => {
            resolve()
          })
        })
    })
  }

  getMyVideos = e => {
    return new Promise((resolve, reject) => {
      let { videos } = this.state
      axios.get(`${process.env.REACT_APP_URI}/api/getmyvideos`, { params: { skipSize: videos.length, userId: this.props.userId } })
        .then((response) => {
          if (response.data) {
            this.setState({
              videos: [...this.state.videos, ...response.data.videos],
              totalLength: response.data.totalLength
            }, () => {
              resolve()
            })
          }
        })
    })
  }

  getMyFollowers = e => {
    return new Promise((resolve, reject) => {
      axios.get(`${process.env.REACT_APP_URI}/api/getmyfollowers`, { withCredentials: true })
        .then((response) => {
          if (response.data) {
            this.setState({
              followers: response.data.followers,
              following: response.data.following
            }, () => {
              resolve()
            })
          }
        })
    })
  }

  setStateMessages = e => {
    this.setState({
      messages: {
        messageProfilUpdated: e
      }
    })
    this.getData()
  }

  newItem = newItem => {
    this.myItemsToSell.getMyItemsToSell(newItem)
  }

  handleOutsideClick = () => {
    this.setState({
      openedGallery: !this.state.openedGallery
    })
  }

  setActive = e => {
    this.setState({
      isActive: e
    })
  }

  handleChange = e => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  handleChangeLinks = e => {
    const { name, value } = e.target
    this.setState({
      links: {
        ...this.state.links,
        [name]: value
      }
    })
  }

  submitBio = e => {
    e.preventDefault()
    axios.post(`${process.env.REACT_APP_URI}/api/addbio`, { aboutMe: this.state.aboutMe }, { withCredentials: true })
      .then((response) => {
        if (response.data === "Ok") {
          this.setState(prevState => ({
            profilData: {
              ...prevState.profilData,
              aboutMe: this.state.aboutMe
            },
            bioExpanded: false,
            aboutMe: ""
          }))
          window.gtag('event', `about me`, {
            'event_category': 'user'
          })
        }
      })
  }

  submitLink = (e, target) => {
    e.preventDefault()
    const linksState = this.state.links
    let targetLink
    if (target === "website") targetLink = linksState.websiteLink
    if (target === "facebook") targetLink = linksState.facebookLink
    if (target === "twitter") targetLink = linksState.twitterLink
    if (target === "instagram") targetLink = linksState.instagramLink
    if (targetLink) {
      axios.post(`${process.env.REACT_APP_URI}/api/addlink`, { target, targetLink }, { withCredentials: true })
        .then((response) => {
          if (response.data === "Ok") {
            this.setState(prevState => ({
              profilData: {
                links: {
                  ...prevState.profilData.links,
                  [target]: targetLink.toLowerCase()
                }
              },
              linksExpanded: false,
              links: {
                websiteLink: "",
                facebookLink: "",
                twitterLink: "",
                instagramLink: ""
              }
            }))
            window.gtag('event', `added link`, {
              'event_category': 'user'
            })
          }
        })
    }
    else {
      this.setState({
        linksExpanded: false
      })
    }
  }

  expandAddBio = e => {
    this.setState({
      bioExpanded: !this.state.bioExpanded
    })
  }

  expandAddLinks = e => {
    if (this.state.linksExpanded != e) {
      this.setState({
        linksExpanded: e
      })
    }
    else if (this.state.linksExpanded === e) {
      this.setState({
        linksExpanded: null
      })
    }
  }

  moreResults() {
    this.getMyVideos()
  }

  removeLinksLink = e => {
    axios.post(`${process.env.REACT_APP_URI}/api/removelinkslink`, { target: e }, { withCredentials: true })
      .then((response) => {
        if (response.data === "Ok") {
          this.setState(prevState => ({
            profilData: {
              links: {
                ...prevState.profilData.links,
                [e]: ""
              }
            },
            bioExpanded: false,
            links: {
              [e]: ""
            }
          }))
          window.gtag('event', `remove link`, {
            'event_category': 'user'
          })
        }
      })
  }

  render() {
    if (!this.props.connected) this.props.history.push("/")
    if (this.state.isLoaded) {
      const videos = this.state.videos
        .map((data) =>
          <div className="post-my-profile" key={data._id}>
            <Link to={`video/${data._id}`}>
              <div className="post-home-container-picture">
                <img src={data.urlThumbnail} alt="thumbnail video" />
                <p className="post-home-pseudo no-margin">{data.title}</p>
                <div className="card-content-home flex justify-space-between">
                  <span className="views-home">{data.views} Views</span>
                  <VotingHome videoId={data._id} likes={data.likes} dislikes={data.dislikes} />
                </div>
              </div>
            </Link>
          </div>
        )
      const followers = this.state.followers
        .map((data) =>
          <li key={data._id} className="followers-list-content">
            <Link to={`user/${data._id}`}><span className="followers-my-profile">{data.username}</span></Link>
          </li>
        )
      const following = this.state.following
        .map((data) =>
          <li key={data._id} className="following-list-content">
            <Link to={`user/${data._id}`}><span className="following-my-profile">{data.username}</span></Link>
          </li>
        )
      return (
        <div>
          <h2 className="my-videos-my-profile-title">{this.state.profilData.username}</h2>
          <div className="cards-home-container-my-profile flex justify-space-between">
            <div className="cards-content-my-profile">
              <h3 className="my-videos-my-profile">My profile</h3>
              <button className="recommendations-button"><span onClick={() => this.setActive("videos")} className={`recommendations-span ${this.state.isActive === "videos" ? "active-recommendations" : ""}`}>Videos</span></button>
              <button className="comments-button"><span onClick={() => this.setActive("timeline")} className={`comments-span ${this.state.isActive === "timeline" ? "active-comments" : ""}`}>Timeline</span></button>
              <div className={`${this.state.isActive === "videos" ? "hide-comments" : ""}`}>
                <h4 className="timeline-title">Timeline</h4>
                <WallUserProfile uploaderId={this.state.profilData._id} connected={this.props.connected} isActive={this.state.isActive} />
              </div>
              <h4 className="bio-title">About me</h4>
              <div className="bio-container">
                <span className="bio-add" onClick={this.expandAddBio}><i className="fas fa-plus"></i> What about me?</span>
                {this.state.profilData.aboutMe && <p className="about-me-content">{this.state.profilData.aboutMe}</p>}
                {this.state.bioExpanded &&
                  <form onSubmit={this.submitBio}>
                    <textarea autoFocus className="about-me-input" name="aboutMe" id="aboutMe" onChange={this.handleChange} value={this.state.aboutMe}></textarea>
                    <br />
                    <button className="save-bio-button" type="submit">Save</button>
                  </form>
                }
              </div>
              <div>
                <h4 className="bio-title">My links</h4>
                <div className="links-container">
                  <div className="links-content">
                    <div className="links-add">
                      <div onClick={() => this.expandAddLinks("website")}>
                        <i className="fas fa-plus"></i>
                        <span> My website</span>
                      </div>
                      {this.state.profilData.links && this.state.profilData.links.website && <span className="my-link">{this.state.profilData.links.website} <i onClick={() => this.removeLinksLink("website")} className="links-link-remove fas fa-times-circle"></i></span>}
                      {this.state.linksExpanded === "website" &&
                        <form onSubmit={(e) => this.submitLink(e, "website")}>
                          <input autoFocus placeholder="https://www.mywebsite.com/" type="text" className="links-about-me-input" name="websiteLink" id="websiteLink" onChange={this.handleChangeLinks} value={this.state.links.websiteLink} />
                          <br />
                          <button className="save-bio-button" type="submit">Save</button>
                        </form>
                      }
                    </div>
                    <div className="links-add">
                      <div onClick={() => this.expandAddLinks("facebook")}>
                        <i className="fas fa-plus"></i>
                        <span> My Facebook</span>
                      </div>
                      {this.state.profilData.links && this.state.profilData.links.facebook && <span className="my-link">{this.state.profilData.links.facebook} <i onClick={() => this.removeLinksLink("facebook")} className="links-link-remove fas fa-times-circle"></i></span>}
                      {this.state.linksExpanded === "facebook" &&
                        <form onSubmit={(e) => this.submitLink(e, "facebook")}>
                          <input autoFocus placeholder="https://www.facebook.com/" type="text" className="links-about-me-input" name="facebookLink" id="facebookLink" onChange={this.handleChangeLinks} value={this.state.links.facebookLink} />
                          <br />
                          <button className="save-bio-button" type="submit">Save</button>
                        </form>
                      }
                    </div>
                    <div className="links-add">
                      <div onClick={() => this.expandAddLinks("twitter")}>
                        <i className="fas fa-plus"></i>
                        <span> My Twitter</span>
                      </div>
                      {this.state.profilData.links && this.state.profilData.links.twitter && <span className="my-link">{this.state.profilData.links.twitter} <i onClick={() => this.removeLinksLink("twitter")} className="links-link-remove fas fa-times-circle"></i></span>}
                      {this.state.linksExpanded === "twitter" &&
                        <form onSubmit={(e) => this.submitLink(e, "twitter")}>
                          <input autoFocus placeholder="https://www.twitter.com/" type="text" className="links-about-me-input" name="twitterLink" id="twitterLink" onChange={this.handleChangeLinks} value={this.state.links.twitterLink} />
                          <br />
                          <button className="save-bio-button" type="submit">Save</button>
                        </form>
                      }
                    </div>
                    <div className="links-add">
                      <div onClick={() => this.expandAddLinks("instagram")}>
                        <i className="fas fa-plus"></i>
                        <span> My Instagram</span>
                      </div>
                      {this.state.profilData.links && this.state.profilData.links.instagram && <span className="my-link">{this.state.profilData.links.instagram} <i onClick={() => this.removeLinksLink("instagram")} className="links-link-remove fas fa-times-circle"></i></span>}
                      {this.state.linksExpanded === "instagram" &&
                        <form onSubmit={(e) => this.submitLink(e, "instagram")}>
                          <input autoFocus placeholder="https://www.instagram.com/" type="text" className="links-about-me-input" name="instagramLink" id="instagramLink" onChange={this.handleChangeLinks} value={this.state.links.instagramLink} />
                          <br />
                          <button className="save-bio-button" type="submit">Save</button>
                        </form>
                      }
                    </div>
                  </div>
                </div>
              </div>
              {videos.length > 0 ?
                <div className={this.state.isActive === "timeline" ? "hide-recommendations" : ""}>
                  <h4 className="videos-title">Videos</h4>
                  {videos}
                  {this.state.videos.length < this.state.totalLength &&
                    <div className="more-button-home-container">
                      <button onClick={() => this.moreResults()} className="more-button-home">More</button>
                    </div>
                  }
                </div>
                :
                <p className="no-videos-yet">No videos yet</p>
              }
            </div>
            <div className="followers-container-my-profile">
              <span className="followers-title-my-profile">Followers</span>
              <ul className="followers-list">{followers}</ul>
              <span className="following-title-my-profile">Following</span>
              <ul className="following-list">{following}</ul>
            </div>
          </div>
        </div>
      )
    }
    else return null
  }
}

export default UserConnexionLoader(onClickOutside(MyProfile))