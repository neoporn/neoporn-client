import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Views from './Views'
import VotingVideo from './voting/VotingVideo'
import RecommendedVideos from './RecommendedVideos'
import userConnexionLoader from './UserConnectionLoader'
import PopUpLogIn from './PopUp/PopUpLogIn'
import Comments from './Comments'
import moment from 'moment-shortformat'

class Video extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            totalLength: null,
            video: null,
            percentageVotes: null,
            onMessage: null,
            uploader: null,
            activeFollow: null,
            followers: null,
            menuConnectionUser: null,
            focused: null,
            isActive: "recommendations",
            showEmbed: false,
            statusPlayPauseBtn: 'pause',
            currentTime: "00:00",
            videoDuration: "00:00",
            videoDurationTotal: 0,
            showControls: false,
            volume: 50,
            muted: false,
            statusFullScreen: false,
            CurrentSeek: 0,
            autoPlay: true,
            showPlayFiligrane: false
        }
        this.video = React.createRef()
        this.progressBar = React.createRef()
        this.playPauseBtn = React.createRef()
        this.embedInput = React.createRef()
        this.contextuelMenuVideo = React.createRef()
        this.videoContainer = React.createRef()
        this.controllers = React.createRef()
        this.volumeSlider = React.createRef()
        this.progressThumb = React.createRef()
    }

    componentDidMount() {
        this.clickTimeout = null
        document.documentElement.classList.add('js')
        axios.get(`${process.env.REACT_APP_URI}/api/getvideo`, { params: { videoId: this.props.match.params.videoid } })
            .then(video => {
                this.setState({
                    loaded: true,
                    video: video.data.video,
                    uploader: video.data.member
                }, () => {
                    document.title = `${video.data.video.title} - neoporn.net`
                    this.video.current.volume = this.state.volume / 100
                })
                axios.get(`${process.env.REACT_APP_URI}/api/getfollowers`, { params: { followedId: this.state.uploader._id } })
                    .then(followers => {
                        this.setState({
                            followers: followers.data.followers,
                            followersNumber: followers.data.followers.length
                        }, () => {
                            this.video.current.addEventListener('durationchange', this.onLoadVideo, false)
                            this.video.current.addEventListener('contextmenu', this.handleContextMenuVideo, false)
                            this.video.current.addEventListener('timeupdate', this.progressBarUpdate, false)
                            this.videoContainer.current.addEventListener('mousemove', this.hoverVideo, false)
                            var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)
                            if (iOS === true) {
                                this.video.current.addEventListener('touchmove', this.hoverVideo, false)
                                this.video.current.addEventListener('touchstart', this.hoverVideo, false)
                                this.video.current.addEventListener('webkitendfullscreen', () => {
                                    this.setState({
                                        statusPlayPauseBtn: "pause",
                                        statusFullScreen: false
                                    })
                                })
                            }
                            this.videoContainer.current.addEventListener("fullscreenchange", () => {
                                this.setState({
                                    statusFullScreen: !this.state.statusFullScreen
                                })
                            });
                            this.videoContainer.current.addEventListener("webkitfullscreenchange", () => {
                                this.setState({
                                    statusFullScreen: !this.state.statusFullScreen
                                })
                            });
                            this.videoContainer.current.addEventListener("webkitexitfullscreen", () => {
                                this.setState({
                                    statusFullScreen: !this.state.statusFullScreen
                                })
                            });
                            this.controllers.current.addEventListener('mouseover', this.leaveControlls, false)
                            this.progressBar.current.addEventListener('mouseover', this.hoverProgressBar, false)
                            document.addEventListener('click', this.removeContextMenuVideo, false)
                            document.addEventListener('click', this.focusVideo, false)
                            this.volumeSlider.current.addEventListener('input', e => {
                                this.volumeSlider.current.style.setProperty('--val', + this.volumeSlider.current.value)
                            }, false);
                            this.progressBar.current.addEventListener('input', e => {
                                this.progressBar.current.style.setProperty('--val', + this.progressBar.current.value)
                            }, false);
                        })
                        const followers2 = this.state.followers.filter(data => {
                            return data.followerId === this.props.userId
                        })
                            .map(data => {
                                return data.followerId
                            })
                        if (followers2.length > 0) {
                            this.setState({
                                activeFollow: true
                            })
                        }
                    })
            })
    }

    componentWillUnmount() {
        clearTimeout(this.t)
        this.video.current.removeEventListener('durationchange', this.onLoadVideo, false)
        this.video.current.removeEventListener('contextmenu', this.handleContextMenuVideo, false)
        this.video.current.removeEventListener('timeupdate', this.progressBarUpdate, false)
        this.videoContainer.current.removeEventListener('mousemove', this.hoverVideo, false)
        this.video.current.removeEventListener('touchmove', this.hoverVideo, false)
        this.video.current.removeEventListener('touchstart', this.hoverVideo, false)
        this.video.current.removeEventListener('webkitendfullscreen', () => {
            this.setState({
                statusPlayPauseBtn: "pause",
                statusFullScreen: false
            })
        })
        this.videoContainer.current.removeEventListener("fullscreenchange", () => {
            this.setState({
                statusFullScreen: !this.state.statusFullScreen
            })
        });
        this.videoContainer.current.removeEventListener("webkitfullscreenchange", () => {
            this.setState({
                statusFullScreen: !this.state.statusFullScreen
            })
        });
        this.videoContainer.current.removeEventListener("webkitexitfullscreen", () => {
            this.setState({
                statusFullScreen: !this.state.statusFullScreen
            })
        });
        this.controllers.current.removeEventListener('mouseover', this.leaveControlls, false)
        this.progressBar.current.removeEventListener('mouseover', this.hoverProgressBar, false)
        document.removeEventListener('click', this.removeContextMenuVideo, false)
        document.removeEventListener('click', this.focusVideo, false)
        this.volumeSlider.current.removeEventListener('input', e => {
            this.volumeSlider.current.style.setProperty('--val', + this.volumeSlider.current.value)
        }, false);
        this.progressBar.current.removeEventListener('input', e => {
            this.progressBar.current.style.setProperty('--val', + this.progressBar.current.value)
        }, false);
    }

    progressBarUpdate = e => {
        let progressBar = this.video.current.currentTime / this.video.current.duration
        this.progressBar.current.style.width = progressBar * 100 + "%"
        let milliseconds = Math.floor(this.video.current.currentTime) * 1000
        let currentTime = moment(moment.duration(milliseconds)._data).format("mm:ss")
        this.setState({
            currentTime: currentTime,
            CurrentSeek: this.video.current.currentTime
        })
    }

    onLoadVideo = e => {
        let milliseconds = Math.floor(this.video.current.duration) * 1000
        let dur = moment(moment.duration(milliseconds)._data).format("mm:ss")
        let showControls
        let onMobile
        let showPlayFiligrane
        if (window.screen.width < 695) {
            onMobile = false
            if (this.video.current.paused) {
                showPlayFiligrane = true
                showControls = true
            }
            else {
                showPlayFiligrane = false
                showControls = false
            }
        }
        else {
            onMobile = true
            if (this.video.current.paused) {
                showPlayFiligrane = true
                showControls = true
            }
            else {
                showPlayFiligrane = false
                showControls = false
            }
        }
        this.setState({
            videoDuration: dur,
            videoDurationTotal: this.video.current.duration,
            autoPlay: onMobile,
            showPlayFiligrane: showPlayFiligrane,
            showControls: showControls
        })
    }

    hoverVideo = e => {
        clearTimeout(this.t)
        this.setState({
            showControls: true
        })
        if (!this.video.current.paused) {
            this.t = setTimeout(() => {
                this.setState({
                    showControls: false
                })
            }, 2000)
        }
    }

    leaveControlls = e => {
        clearTimeout(this.t)
    }

    handleVolume = e => {
        this.setState({
            volume: e.target.value
        }, () => {
            this.video.current.volume = this.state.volume / 100
        })
    }

    vidSeek = e => {
        this.setState({
            CurrentSeek: e.target.value
        }, () => {
            this.video.current.currentTime = this.state.CurrentSeek
        })
    }

    handleContextMenuVideo = e => {
        e.preventDefault()
        const videoX = this.video.current.offsetWidth
        const mousePositionInVideo = e.pageX - this.video.current.offsetLeft
        if (mousePositionInVideo >= (videoX - 140)) {
            this.contextuelMenuVideo.current.style.display = "block";
            this.contextuelMenuVideo.current.style.left = (e.pageX - 140) + "px";
            this.contextuelMenuVideo.current.style.top = e.pageY + "px";
        }
        else {
            this.contextuelMenuVideo.current.style.display = "block";
            this.contextuelMenuVideo.current.style.left = e.pageX + "px";
            this.contextuelMenuVideo.current.style.top = e.pageY + "px";
        }
    }

    removeContextMenuVideo = e => {
        this.contextuelMenuVideo.current.style.display = "none";
    }

    pauseVideo = event => {
        if (event.keyCode === 32) {
            if (this.video.current) {
                event.preventDefault()
                this.focused(true)
            }
        }
    }

    follow = e => {
        if (this.props.connected) {
            axios.post(`${process.env.REACT_APP_URI}/api/follow`, { followedId: this.state.uploader._id, videoId: this.state.video._id }, { withCredentials: true })
                .then(response => {
                    if (response.data === 'Followed') {
                        this.setState({
                            activeFollow: true,
                            followersNumber: this.state.followersNumber + 1
                        })
                    }
                    else if (response.data === 'Unfollowed') {
                        this.setState({
                            activeFollow: false,
                            followersNumber: this.state.followersNumber - 1
                        })
                    }
                })
        }
        else {
            this.setState({
                menuConnectionUser: !this.state.menuConnectionUser
            })
        }
    }

    handleClickOutside = e => {
        this.handleOutsideClick()
    }

    handleOutsideClick = () => this.setState({
        menuConnectionUser: false
    })

    toggleMenuConnectionUser = (e) => {
        e.stopPropagation()
        e.preventDefault()

        this.setState({
            menuConnectionUser: !this.state.menuConnectionUser
        })
    }

    handleClicks = (state) => {
        if (this.clickTimeout !== null) {
            this.fullScreen()
            clearTimeout(this.clickTimeout)
            this.clickTimeout = null
        } else {
            this.clickTimeout = setTimeout(() => {
                this.focused(state)
                clearTimeout(this.clickTimeout)
                this.clickTimeout = null
            }, 300)
        }
    }

    focusVideo = e => {
        let targetElement = e.target
        do {
            if (targetElement == this.videoContainer.current) {
                document.addEventListener('keydown', this.pauseVideo, false)
                return
            }
            targetElement = targetElement.parentNode
        } while (targetElement)
        document.removeEventListener('keydown', this.pauseVideo, false)
    }

    focused = state => {
        if (window.screen.width >= 695) {
            if (state === false) {
                this.video.current.blur()
            }
            else {
                this.video.current.focus()
                if (this.video.current.paused) {
                    this.setState({
                        statusPlayPauseBtn: "play"
                    })
                    this.video.current.play()
                }
                else {
                    this.setState({
                        statusPlayPauseBtn: "pause"
                    })
                    this.video.current.pause()
                }
            }
        }
    }

    setActive = e => {
        this.setState({
            isActive: e
        })
    }

    showEmbed = e => {
        this.setState({
            showEmbed: !this.state.showEmbed
        })
    }

    focus = e => {
        this.embedInput.current.focus()
    }

    select = e => {
        this.embedInput.current.select()
        window.gtag('event', 'Embed link selected', {
            'event_category': 'user',
            'event_label': `${this.state.video.title}`
        })
    }

    copyURL = e => {
        const el = document.createElement('textarea')
        el.value = window.location.href
        el.setAttribute('readonly', '')
        el.style.position = 'absolute'
        el.style.left = '-9999px'
        document.body.appendChild(el)
        el.select()
        document.execCommand('copy')
        document.body.removeChild(el)
    }

    copyEmbedURL = e => {
        const el = document.createElement('textarea')
        el.value = `<iframe width="510" height="400" src="${process.env.REACT_APP_URI}/embed/${this.state.video._id}" frameborder="0" scrolling="no" allowfullscreen></iframe>`
        el.setAttribute('readonly', '')
        el.style.position = 'absolute'
        el.style.left = '-9999px'
        document.body.appendChild(el)
        el.select()
        document.execCommand('copy')
        document.body.removeChild(el)
    }

    togglePlayPauseBtn = e => {
        if (this.video.current.paused) {
            this.setState({
                statusPlayPauseBtn: "play"
            })
            this.video.current.play()
        }
        else {
            this.setState({
                statusPlayPauseBtn: "pause"
            })
            this.video.current.pause()
        }
    }

    togglePlayPauseBtnNoAction = e => {
        if (this.video.current.paused) {
            clearTimeout(this.t)
            this.setState({
                statusPlayPauseBtn: "pause",
                showPlayFiligrane: true,
                showControls: true
            })
        }
        else {
            this.setState({
                statusPlayPauseBtn: "play",
                showPlayFiligrane: false,
                showControls: false
            })
        }
    }

    fullScreen = e => {
        if (this.state.statusFullScreen === false) {
            let element = this.videoContainer.current
            if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
                if (this.video.current.webkitEnterFullscreen) this.video.current.webkitEnterFullscreen()
            }
            if (element.requestFullscreen) element.requestFullscreen()
            else if (element.webkitRequestFullscreen) element.webkitRequestFullscreen()
            else if (element.mozRequestFullScreen) element.mozRequestFullScreen()

        }
        else {
            if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
                if (this.video.current.webkitExitFullscreen) this.video.current.webkitExitFullscreen()
            }
            if (document.exitFullscreen) document.exitFullscreen()
            else if (document.webkitExitFullscreen) document.webkitExitFullscreen()
            else if (document.mozCancelFullScreen) document.mozCancelFullScreen()
            else if (document.msExitFullScreen) document.msExitFullScreen()
        }
    }

    mute = () => {
        this.setState({
            muted: !this.state.muted
        })
    }

    render() {
        if (this.state.loaded) {
            let tags = this.state.video.tags.map((tag, index) =>
                <Link to={`/tag/${tag}`} className="tag-link" key={index}><span>{tag}</span></Link>
            )
            return (
                <div className="video-container flex justify-space-between">
                    <div className="video-content">
                        <div className="c-video" ref={this.videoContainer}>
                            <Views videoId={this.props.match.params.videoid} videoName={this.state.video.title}>
                                <video onPlay={this.togglePlayPauseBtnNoAction} onPause={this.togglePlayPauseBtnNoAction} playsInline poster={this.state.video.urlThumbnail} muted={this.state.muted} onClick={() => this.handleClicks()} onBlur={() => this.focused(false)} ref={this.video} width="100%" height="auto" controlsList="nodownload" autoPlay={this.state.autoPlay} preload="metadata">
                                    <source src={this.state.video.url} type="video/mp4" />
                                </video>
                            </Views>
                            <div className="play-filigrane">
                                {this.state.showPlayFiligrane && <i onClick={this.togglePlayPauseBtn} className="fas fa-play"></i>}
                            </div>
                            <div ref={this.controllers} className={`controls-video-player ${this.state.showControls ? "hover-video" : ""}`}>
                                <div className="progress-bar-container">
                                    <input ref={this.progressBar} style={{ '--min': '0', '--max': this.state.videoDurationTotal, '--val': this.state.CurrentSeek }} className="seek-slider-input seek-custom-range" type="range" min="0" max={this.state.videoDurationTotal} id="myRangeSeek" onChange={this.vidSeek} value={this.state.CurrentSeek} />
                                </div>
                                <div className="buttons">
                                    {this.state.statusPlayPauseBtn === "pause" ?
                                        <button ref={this.playPauseBtn} onClick={this.togglePlayPauseBtn} className="play-pause-button" aria-label="Play"><i className="fas fa-play"></i></button>
                                        :
                                        <button ref={this.playPauseBtn} onClick={this.togglePlayPauseBtn} className="play-pause-button" aria-label="Pause"><i className="fas fa-pause"></i></button>
                                    }
                                </div>
                                <div className="current-time">
                                    <span className="current-time-input">{this.state.currentTime} / {this.state.videoDuration}</span>
                                </div>
                                <div className="mute">
                                    {this.state.muted ?
                                        <button onClick={this.mute}><i className="fas fa-volume-off"></i></button>
                                        :
                                        <button onClick={this.mute}><i className="fas fa-volume-up"></i></button>
                                    }
                                </div>
                                <div className="volume-slider">
                                    <input ref={this.volumeSlider} style={{ '--min': '0', '--max': '100', '--val': '50' }} className="volume-slider-input custom-range" type="range" min="0" max="100" id="myRange" onChange={this.handleVolume} value={this.state.volume} />
                                </div>
                                <div className="buttons expand-button">
                                    <button ref={this.playPauseBtn} onClick={this.fullScreen} className="play-pause-button" aria-label="Play"><i className="fas fa-expand"></i></button>
                                </div>
                            </div>
                        </div>
                        <ul className="contextuel-menu-video" ref={this.contextuelMenuVideo}>
                            <li onClick={this.copyURL} className="contextuel-menu-video-link">Copy Video URL</li>
                            <li onClick={this.copyEmbedURL} className="contextuel-menu-video-link">Copy Embed URL</li>
                        </ul>
                        <div className="infos-video flex justify-space-between">
                            <div>
                                <p className="title-video">{this.state.video.title}</p>
                                <p className="description-video">{this.state.video.description}</p>
                            </div>
                            <div className="uploader-username-video">
                                {this.state.uploader._id != this.props.userId && <button className="follow-button" onClick={this.follow}>{this.state.activeFollow ? "Following" : "Follow"} {this.state.followersNumber}</button>}
                                <p className="author-video">By <Link className="to-uploader-link-video" to={`/user/${this.state.uploader._id}`}>{this.state.uploader.username}</Link></p>
                            </div>
                        </div>
                        <div className="flex justify-space-between tags-embed-container">
                            <div className="tags-video">
                                {tags}
                            </div>
                            <div className="embed-button">
                                <div onClick={this.showEmbed} className="embed-button-containt">
                                    <i className="fas fa-code"></i>
                                    <span> Embed</span>
                                </div>
                            </div>
                        </div>
                        {this.state.showEmbed &&
                            <div className="share-container">
                                <input onClick={() => { this.focus(); this.select(); }} ref={this.embedInput} style={{ color: 'black' }} readOnly value={`<iframe width="510" height="287" src="${process.env.REACT_APP_URI}/embed/${this.state.video._id}" frameborder="0" scrolling="no" allowfullscreen></iframe>`} />
                            </div>
                        }
                        <hr color="white" width="100%" />
                        <div className="card-content-home">
                            <VotingVideo views={this.state.video.views} videoId={this.props.match.params.videoid} videoName={this.state.video.title} likes={this.state.video.likes} dislikes={this.state.video.dislikes} />
                        </div>
                        <button className="recommendations-button"><span onClick={() => this.setActive("recommendations")} className={`recommendations-span ${this.state.isActive === "recommendations" ? "active-recommendations" : ""}`}>Recommendations</span></button>
                        <button className="comments-button"><span onClick={() => this.setActive("comments")} className={`comments-span ${this.state.isActive === "comments" ? "active-comments" : ""}`}>Comments</span></button>
                        <div className={`comments-parent-mobile ${this.state.isActive === "recommendations" ? "hide-comments" : ""}`}><Comments videoId={this.state.video._id} connected={this.props.connected} /></div>
                    </div>
                    {this.state.menuConnectionUser &&
                        <div className="logInHeader">
                            <PopUpLogIn handleClickOutside={this.handleClickOutside} toAction="to follow" />
                        </div>
                    }
                    <div className={`recomanded-videos ${this.state.isActive === "comments" ? "hide-recommendations" : ""}`}>
                        <RecommendedVideos />
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default userConnexionLoader(Video)