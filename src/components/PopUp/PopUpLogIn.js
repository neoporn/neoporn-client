//React
import React from 'react'
//Router
import { withRouter } from 'react-router-dom'
import axios from 'axios'

class PopUpLogin extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            containerOn: 'logIn',
            formDataSignIn: {
                email: "",
                password: ""
            },
            formDataSignUp: {
                email: "",
                username: "",
                password: "",
                retypePassword: "",
                termsConditions: ""
            },
            forgotPassword: "",
            messages: {
                messageStatusSignIn: "",
                messageStatusSignUp: ""
            }
        }
    }

    componentDidMount() {
        document.body.classList.add('noScrollBody')
    }

    componentWillUnmount() {
        document.body.classList.remove('noScrollBody')
    }

    handleClickOutside = e => {
        e.stopPropagation();
        e.preventDefault();

        this.props.handleClickOutside()
    }

    setContainer = container => {
        this.setState({
            containerOn: container
        })
    }

    handleChangeSignIn = e => {
        const { value, name } = e.target
        const { formDataSignIn } = this.state
        this.setState({
            formDataSignIn: {
                ...formDataSignIn,
                [name]: value
            }
        })
    }

    handleChangeSignUp = e => {
        const { name } = e.target
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
        const { formDataSignUp } = this.state
        this.setState({
            formDataSignUp: {
                ...formDataSignUp,
                [name]: value
            }
        })
    }

    handleChangePassword = e => {
        const { name, value } = e.target
        const { forgotPassword } = this.state
        this.setState({
            [name]: value
        })
    }

    handleFileUpload = e => {
        const { name } = e.target
        const { formDataSignUp } = this.state
        let reader = new FileReader()
        let file = e.target.files[0]
        reader.onload = () => {
            this.setState({
                formDataSignUp: {
                    ...formDataSignUp,
                    [name]: reader.result
                }
            })
        }
        reader.readAsDataURL(file)
    }

    signInForm = (e) => {
        e.preventDefault()

        axios.post(`${process.env.REACT_APP_URI}/api/signin`, this.state.formDataSignIn, {
            withCredentials: true
        })
            .then(data => {
                if (data.data == "A required field is missing") {
                    this.setState({
                        messages: {
                            messageStatusSignIn: data.data
                        }
                    })
                }
                if (data.data == "Wrong password") {
                    this.setState({
                        messages: {
                            messageStatusSignIn: data.data
                        }
                    })
                }
                if (data.data == "No account founded") {
                    this.setState({
                        messages: {
                            messageStatusSignIn: data.data
                        }
                    })
                }
                if (data.data == "Confirm email address") {
                    this.setState({
                        messages: {
                            messageStatusSignIn: "Please confirm your email address to sign in"
                        }
                    })
                }
                if (data.data == "Ok") {
                    this.props.handleClickOutside()
                    window.location.reload()
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    signUpForm = (e) => {
        e.preventDefault()

        axios.post(`${process.env.REACT_APP_URI}/api/signup`, this.state.formDataSignUp, {
            withCredentials: true
        })
            .then(data => {
                if (data.data == "A required field is missing") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "Age value must be a number") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "Passwords do not match") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "Password too short") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: "Your password is too short, minimum 6 characters"
                        }
                    })
                }
                else if (data.data == "This email address already exist") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "This username already exist") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "Invalid email address format") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "You have to accept the terms and conditions") {
                    this.setState({
                        messages: {
                            messageStatusSignUp: data.data
                        }
                    })
                }
                else if (data.data == "Ok") {
                    this.setContainer("confirmEmail")
                    window.gtag('event', `Sign up`, {
                        'event_category': 'user',
                        'event_label': `Signed up`
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    forgotPassword = e => {
        e.preventDefault()

        axios.get(`${process.env.REACT_APP_URI}/api/forgotpassword`, { params: { email: this.state.forgotPassword } })
            .then(data => {
                if (data.data == "No account found") {
                    this.setState({
                        messages: {
                            messageStatusPassword: data.data
                        }
                    })
                }
                else if (data.data == "Ok") {
                    this.setState({
                        messages: {
                            messageStatusPassword: "Sent! You should receive the email soon!"
                        }
                    })
                    window.gtag('event', `Password reset`, {
                        'event_category': 'user',
                        'event_label': `Sent`
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        let { containerOn } = this.state
        const { handleClickOutside } = this.props
        return (
            <div className="popUpLog">
                {containerOn == 'logIn' && <div className="container-pop-up-login">
                    <div className="background-pop-up-login" onClick={handleClickOutside}></div>
                    <div className="pop-up-log-to">
                        <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
                        <h2 className="text-align-center title-log-to">Login or sign up {this.props.toAction}</h2>
                        <div className="pop-up-lo-to-left">
                            <form className="form-sign-up form-group" ref={input => this.signInInput = input}
                                onSubmit={e => this.signInForm(e)}>
                                <input type="text" className="form-control" name="email" placeholder="Email / Username" value={this.state.formDataSignIn.email} onChange={this.handleChangeSignIn} required />
                                <input type="password" className="form-control" name="password" placeholder="Password" value={this.state.formDataSignIn.password} onChange={this.handleChangeSignIn} required />
                                <div className="button-form-container">
                                    <button type="submit" className="btn btn-primary button-pop-up">Log in</button>
                                    <p className="link-forgot-password no-margin" onClick={() => this.setContainer("forgotPassword")}>Forgot your password?</p>
                                </div>
                            </form>
                            <p className="error-message-signup">{this.state.messages.messageStatusSignIn}</p>
                        </div>
                        <div className="pop-up-lo-to-right">
                            <h3 className="title-log-to-right">Not a free member yet?</h3>
                            <span className="second-title-log-to-right">Here's what you're missing out on!</span>
                            <div className="pop-up-log-to-list-missing flex">
                                <div className="margin-auto-center">
                                    <p className="pop-up-log-to-missing"><i className="fas fa-check check-icon"></i> Post comments</p>
                                    <p className="pop-up-log-to-missing"><i className="fas fa-check check-icon"></i> Like/Dislike</p>
                                    <p className="pop-up-log-to-missing"><i className="fas fa-check check-icon"></i> Upload videos</p>
                                    <p className="pop-up-log-to-missing pop-up-log-to-missing-many-more"> And many more!</p>
                                </div>
                            </div>
                            <button className="btn btn-primary button-pop-up-sign-up" onClick={() => this.setContainer("buyerForm")}>Sign up</button>
                        </div>
                    </div>
                </div>}

                {containerOn == 'forgotPassword' && <div className="container-pop-up-login">
                    <div className="background-pop-up-login" onClick={handleClickOutside}></div>
                    <div className="pop-up-log-to">
                        <span onClick={() => this.setContainer("logIn")} className="chevron-left-pop-up"><i className="fas fa-chevron-left"></i></span>
                        <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
                        <h2 className="text-align-center clear-both forgot-password-title">You forgot your password?<br />No worries!</h2>
                        <p className="text-align-center forgot-password-text">Please enter your email address and we will send you instructions on how to reset your password</p>
                        <form className="form-forgot-password form-group" onSubmit={e => this.forgotPassword(e)}>
                            <input type="text" className="form-control forgot-password-input" id="pseudo" placeholder="Email" name="forgotPassword" value={this.state.forgotPassword} onChange={this.handleChangePassword} required />
                            <div className="button-form-container forgot-password-button">
                                <button type="submit" className="btn btn-primary button-pop-up">Send</button>
                            </div>
                        </form>
                        <p className="error-message-signup">{this.state.messages.messageStatusPassword}</p>
                    </div>
                </div>}

                {containerOn == 'buyerForm' && <div className="container-pop-up-login">
                    <div className="background-pop-up-login" onClick={handleClickOutside}></div>
                    <div className="pop-up-log-to">
                        <span onClick={() => this.setContainer("logIn")} className="chevron-left-pop-up"><i className="fas fa-chevron-left"></i></span>
                        <i className="fas fa-times close-pop-up-login" onClick={handleClickOutside}></i>
                        <h2 className="text-align-center clear-both">Sign up</h2>
                        <p className="sign-up-text">
                            Members can comment, like/dislike, follow, upload and more
                            <br />
                            It's free, fast and safe
                            <br />
                            Your password it's encrypted even us can't see it
                            <br />
                            We will never send any spam
                        </p>
                        <form className="form-sign-up form-group" ref={input => this.signUpInput = input} onSubmit={e => this.signUpForm(e)}>
                            <input type="text" className="form-control" id="email" name="email" placeholder="Email *" value={this.state.formDataSignUp.email} onChange={this.handleChangeSignUp} required />
                            <input type="text" className="form-control" id="username" name="username" placeholder="Username *" value={this.state.formDataSignUp.username} onChange={this.handleChangeSignUp} required />
                            <input type="password" className="form-control" id="password" name="password" placeholder="Password *" minLength="6" value={this.state.formDataSignUp.password} onChange={this.handleChangeSignUp} required />
                            <input type="password" className="form-control" id="retype-password" name="retypePassword" placeholder="Retype password *" value={this.state.formDataSignUp.retypePassword} onChange={this.handleChangeSignUp} required />
                            <input type="checkbox" id="termsconditions" name="termsConditions" checked={this.state.formDataSignUp.termsConditions} onChange={this.handleChangeSignUp} required />
                            <label htmlFor="termsconditions" className="label-terms-conditions">Accepted terms and conditions.</label>
                            <button type="submit" className="btn btn-primary button-pop-up">Sign up</button>
                        </form>
                        <p className="error-message-signup">{this.state.messages.messageStatusSignUp}</p>
                    </div>
                </div>}
                {containerOn == 'confirmEmail' && <div className="container-pop-up-login">
                    <div className="background-pop-up-login" onClick={handleClickOutside}></div>
                    <div className="pop-up-sign-up-buyer">
                        <span onClick={() => this.setContainer("logIn")} className="chevron-left-pop-up"><i className="fas fa-chevron-left"></i></span>
                        <button className="close-pop-up-login" onClick={handleClickOutside}>X</button>
                        <div className="success-button clear-both">
                            <i className="fas fa-check-circle"></i>
                            <h4 className="title-confirm-email">Hi Username,</h4>
                            <p className="text-confirm-email">Please confirm your address email <br />
                                to make sure we got it right.
                            </p>
                            <span>See you soon!</span>
                        </div>
                    </div>
                </div>}
            </div>
        )
    }
}

export default withRouter(PopUpLogin)