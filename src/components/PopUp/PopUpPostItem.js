//React
import React from 'react'
import axios from 'axios'
//Components
import UserConnexionLoader from '../UserConnectionLoader'
import { Redirect } from 'react-router-dom'

class PopUpPostItem extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      containerOn: this.props.showModify,
      formData: {
        name: "",
        description: "",
        img: "",
        price: "",
        notif: false,
        directSell: false
      },
      messages: {
        messageStatus: ""
      }
    }
  }

  checkboxChange = e => {
    const { name } = e.target
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: !formData[name]
      }
    })
  }

  handleChange = e => {
    const { value, name } = e.target
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    const { name } = e.target
    const { formData } = this.state
    let reader = new FileReader()
    let file = e.target.files[0]
    reader.onload = () => {
      this.setState({
        formData: {
          ...formData,
          [name]: reader.result
        }
      })
    }
    reader.readAsDataURL(file)
  }

  createPost = e => {
    e.preventDefault()

    axios.post(`${process.env.REACT_APP_URI}/api/postproduit`, this.state.formData, {
      withCredentials: true
    })
      .then(data => {
        this.props.setStateMessages(data.data)
        this.props.newItem(this.state.formData)
        this.props.closePopUpModify()
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { name, description, img, price, notif } = this.state.formData
    return (
      <div className="popUpLog">
        {this.state.containerOn == 'postItem' && <div className="container-pop-up-modify-my-item">
          <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}></div>
          <div className="pop-up-modify-my-item">
            <button className="close-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}>X</button>
            <h2 className="text-align-center">Post your item</h2>
            <form className="post-form" ref={input => this.postForm = input} onSubmit={e => this.createPost(e)}>
              <div className="form-group">
                <input type="text" name="name" className="form-control" value={name} onChange={this.handleChange} placeholder="Name" required />
              </div>
              <div className="form-group">
                <input type="text" name="description" className="form-control" value={description} onChange={this.handleChange} placeholder="Description" required />
              </div>
              <div className="form-group">
                <input type="file" name="img" className="form-control" onChange={this.handleFileUpload} required />
              </div>
              <div className="input-group">
                <span className="input-group-addon">$</span>
                <input type="number" className="form-control" name="price" step="any" min="0" value={price} onChange={this.handleChange} placeholder="Price" aria-label="Amount (to the nearest dollar)" required />
              </div>
              <div className="form-check">
                <input type="checkbox" name="notif" id="get-notified-by-email-post" checked={notif} onChange={this.checkboxChange} className="intimy-checkbox" ref={input => this.notif = input} />
                <label htmlFor="get-notified-by-email-post"> Get notified by email</label>
              </div>
              <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
            </form>
          </div>
        </div>}
      </div>
    )
  }
}

export default UserConnexionLoader(PopUpPostItem)