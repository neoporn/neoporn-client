//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import axios from 'axios'
import onClickOutside from 'react-onclickoutside'

class PopUpModifyMyProfile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      popUpModifyMyProfile: false,
      errorMessage: "",
      modifyMyProfileSeller: {
        profilPicture: "",
        username: ""
      }
    }
  }
  
  componentDidMount() {
    document.body.classList.add('noScrollBody')
  }
  
  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  handleChangeSeller = e => {
    const {value, name} = e.target
    const {modifyMyProfileSeller} = this.state
    let interestedInArray = [...modifyMyProfileSeller.interestedIn]
    if (e.target.type == "checkbox") {
      if (!e.target.checked) {
        var index = interestedInArray.indexOf(e.target.value)
        interestedInArray.splice(index, 1)
      }
      else {
        interestedInArray = [...modifyMyProfileSeller.interestedIn, value]
      }
    }
    this.setState({
      modifyMyProfileSeller: {
        ...modifyMyProfileSeller,
        [name]: e.target.type === "checkbox" ? interestedInArray : value
      }
    })
  }

  handleChangeBuyer = e => {
    const {value, name} = e.target
    const {modifyMyProfileBuyer} = this.state
    this.setState({
      modifyMyProfileBuyer: {
        ...modifyMyProfileBuyer,
        [name]: value
      }
    })
  }

  modifyMyProfile = e => {
    e.preventDefault()
    let editedProfil

    if (this.props.userStatus == "seller") {
      editedProfil = this.state.modifyMyProfileSeller
    }
    if (this.props.userStatus == "buyer") {
      editedProfil = this.state.modifyMyProfileBuyer
    }

    axios.put(`${process.env.REACT_APP_URI}/api/modifyMyProfile`, editedProfil, {
      withCredentials: true
    })
    .then(data => {
      if (data.data == "Profil updated!") {
        this.props.setStateMessages(data.data)
        this.props.closePopUpModify()
      }
      else if (data.data == "Wrong ext") {
        this.setState({
          errorMessage: "Only jpeg, png, gif files are allowed"
        })
      }
    })
    .catch(error => {
      console.log(error)
    })
  }

  handleFileUpload = e => {
    e.preventDefault()
    const {name} = e.target
    const {modifyMyProfileSeller, modifyMyProfileBuyer} = this.state
    let files = Array.from(e.target.files)
    let filesResults = []
    let attributeName = (() => {
      if (this.props.userStatus == "seller") return "modifyMyProfileSeller"
      else if (this.props.userStatus == "buyer") return "modifyMyProfileBuyer"
      return null
    })()
    
    let promise = files.map(file => new Promise(resolve => {
      let reader  = new FileReader()
      reader.onloadend = () => {
        filesResults.push(reader.result)
        resolve()
      }

      reader.readAsDataURL(file)
    }))

    Promise.all(promise).then(() => {
      this.setState({
        [attributeName]: {
          ...this.state[attributeName],
          [name]: filesResults
        }
      })
    })
  }

  render() {
    return (
      <div className="popUpLog">
         <div className="container-pop-up-modify-my-buyer-profil">
          <div className="background-pop-up-modify-my-buyer-profil" onClick={() => this.props.closePopUpModify()}></div>
          <div className="pop-up-sign-up-buyer">
            <button className="close-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}>X</button>
            <h2 className="text-align-center">Edit my profile</h2>
            {this.props.userStatus == "seller" && <form className="form-sign-up form-group post-form" ref={input => this.postFormSeller = input} onSubmit={e => this.modifyMyProfile(e)}>
              <label htmlFor="profil-picture">Profil picture</label>      
              <input type="file" name="profilPicture" className="form-control" onChange={this.handleFileUpload} accept="image/png, image/jpeg" />
              <label htmlFor="pictures">Upload pictures</label>          
              <input type="file" name="pictures" className="form-control" onChange={this.handleFileUpload} accept="image/png, image/jpeg" multiple />
              <input type="text" name="username" className="form-control" value={this.state.modifyMyProfileSeller.username} onChange={this.handleChangeSeller} placeholder="Username" />
              <textarea type="text" name="bio" className="form-control" value={this.state.modifyMyProfileSeller.bio} onChange={this.handleChangeSeller} placeholder="Bio"></textarea>
              <select name="hairs" id="hairs" className="form-control" value={this.state.modifyMyProfileSeller.hairs} onChange={this.handleChangeSeller}>
                <option value="" disabled hidden>Hairs</option>
                <option value="Blonde">Blonde</option>
                <option value="Brown">Brown</option>
                <option value="Black">Black</option>
                <option value="Red">Red</option>
                <option value="Silver">Silver</option>
                <option value="Colored">Colored</option>
              </select>
              <input type="number" name="height" className="form-control" value={this.state.modifyMyProfileSeller.height} onChange={this.handleChangeSeller} placeholder="Height" />
              <input type="number" name="weight" className="form-control" value={this.state.modifyMyProfileSeller.weight} onChange={this.handleChangeSeller} placeholder="Weight" />
              <select name="eyes" id="eyes" className="form-control" value={this.state.modifyMyProfileSeller.eyes} onChange={this.handleChangeSeller}>
                <option value="" disabled hidden>Eyes</option>
                <option value="Blue">Blue</option>
                <option value="Brown">Brown</option>
                <option value="Green">Green</option>
                <option value="Hazel">Hazel</option>
                <option value="Silver">Silver</option>
                <option value="Amber">Amber</option>
              </select>
              <select name="status" id="status" className="form-control" value={this.state.modifyMyProfileSeller.status} onChange={this.handleChangeSeller}>
                <option value="" disabled hidden>Status</option>
                <option value="Single">Single</option>
                <option value="Couple">Couple</option>
                <option value="Nothing serious">Nothing serious</option>
              </select>
              <div className="radio-group">
                <p>Interested in:</p>
                <div className="form-check form-check-inline">
                  <input className="form-check-input radio" type="checkbox" id="interestedInMen" name="interestedIn" value="Men" onChange={this.handleChangeSeller}/>
                  <label className="form-check-label" htmlFor="interestedInMen">Men</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input radio" type="checkbox" id="interestedInWomen" name="interestedIn" value="Women" onChange={this.handleChangeSeller}/>
                  <label className="form-check-label" htmlFor="interestedInWomen">Women</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input radio" type="checkbox" id="interestedInCouple" name="interestedIn" value="Couple" onChange={this.handleChangeSeller}/>
                  <label className="form-check-label" htmlFor="interestedInCouple">Couple</label>
                </div>
                <div className="form-check form-check-inline">
                  <input className="form-check-input radio" type="checkbox" id="interestedIntrans" name="interestedIn" value="Trans" onChange={this.handleChangeSeller}/>
                  <label className="form-check-label" htmlFor="interestedIntrans">Trans</label>
                </div>
              </div>
              <input type="text" name="measurements" className="form-control" value={this.state.modifyMyProfileSeller.measurements} onChange={this.handleChangeSeller} placeholder="Measurements" />
              <div className="radio-group">
                <div className="form-check form-check-inline">
                  <span className="legend-radio-form">Tattoos?</span>
                  <input type="radio" name="tattoos" className="form-check-input radio" id="tattoos-yes" value="Yes" onChange={this.handleChangeSeller} />
                  <label htmlFor="tattoos-yes" className="form-check-label">Yes</label>
                </div>
                <div className="form-check form-check-inline">
                  <input type="radio" name="tattoos" className="form-check-input radio" id="tattoos-no" value="No" onChange={this.handleChangeSeller} />
                  <label htmlFor="tattoos-no" className="form-check-label">No</label>
                </div>
              </div>
              <div className="radio-group">
                <div className="form-check form-check-inline">
                  <span className="legend-radio-form">Piercings?</span>
                  <input type="radio" name="piercings" className="form-check-input radio" id="piercings-yes" value="Yes" onChange={this.handleChangeSeller} />
                  <label htmlFor="piercings-yes" className="form-check-label">Yes</label>
                </div>
                <div className="form-check form-check-inline">
                  <input type="radio" name="piercings" className="form-check-input radio" id="piercings-no" value="No" onChange={this.handleChangeSeller} />
                  <label htmlFor="piercings-no" className="form-check-label">No</label>
                </div>
              </div>
              <input type="text" name="whatILike" className="form-control" value={this.state.modifyMyProfileSeller.whatILike} onChange={this.handleChangeSeller} placeholder="What I like" />
              <input type="text" name="whatIDontLike" className="form-control" value={this.state.modifyMyProfileSeller.whatIDontLike} onChange={this.handleChangeSeller} placeholder="What I don't like" />
              <br/>
              <h3>My shipping informations</h3>
              <input type="text" name="firstname" className="form-control" value={this.state.modifyMyProfileSeller.firstname} onChange={this.handleChangeSeller} placeholder="First Name" />
              <input type="text" name="lastname" className="form-control" value={this.state.modifyMyProfileSeller.lastname} onChange={this.handleChangeSeller} placeholder="Last Name" />
              <input type="text" name="streetaddress" className="form-control" value={this.state.modifyMyProfileSeller.streetaddress} onChange={this.handleChangeSeller} placeholder="Street Address" />
              <input type="text" name="apt" className="form-control" value={this.state.modifyMyProfileSeller.apt} onChange={this.handleChangeSeller} placeholder="Apt #, Floor, etc..." />
              <input type="text" name="city" className="form-control" value={this.state.modifyMyProfileSeller.city} onChange={this.handleChangeSeller} placeholder="City" />
              <input type="text" name="state" className="form-control" value={this.state.modifyMyProfileSeller.state} onChange={this.handleChangeSeller} placeholder="State" />
              <input type="text" name="zipcode" className="form-control" value={this.state.modifyMyProfileSeller.zipcode} onChange={this.handleChangeSeller} placeholder="Zip Code" />
              <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
              <br/>
              {this.state.errorMessage}
            </form>}

            {this.props.userStatus == "buyer" && <form className="form-sign-up form-group post-form" ref={input => this.postFormBuyer = input} onSubmit={e => this.modifyMyProfile(e)}>
              <label htmlFor="profil-picture">Profil picture</label>          
              <input type="file" name="profilpicture" className="form-control" onChange={this.handleFileUpload} />
              <input type="text" name="username" className="form-control" value={this.state.modifyMyProfileBuyer.username} onChange={this.handleChangeBuyer} placeholder="Username" />
              <h3>My shipping informations</h3>
              <input type="text" name="firstname" className="form-control" value={this.state.modifyMyProfileBuyer.firstname} onChange={this.handleChangeBuyer} placeholder="First Name" />
              <input type="text" name="lastname" className="form-control" value={this.state.modifyMyProfileBuyer.lastname} onChange={this.handleChangeBuyer} placeholder="Last Name" />
              <input type="text" name="streetaddress" className="form-control" value={this.state.modifyMyProfileBuyer.streetaddress} onChange={this.handleChangeBuyer} placeholder="Street Address" />
              <input type="text" name="apt" className="form-control" value={this.state.modifyMyProfileBuyer.apt} onChange={this.handleChangeBuyer} placeholder="Apt #, Floor, etc... (Optional)" />
              <input type="text" name="city" className="form-control" value={this.state.modifyMyProfileBuyer.city} onChange={this.handleChangeBuyer} placeholder="City" />
              <input type="text" name="state" className="form-control" value={this.state.modifyMyProfileBuyer.state} onChange={this.handleChangeBuyer} placeholder="State" />
              <input type="text" name="zipcode" className="form-control" value={this.state.modifyMyProfileBuyer.zipcode} onChange={this.handleChangeBuyer} placeholder="Zip Code" />
              <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
            </form>}
          </div>
        </div>
      </div>
    )
  }
}

export default onClickOutside(PopUpModifyMyProfile)