//React
import React, { Component } from 'react'
import axios from 'axios'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

class PopUpModifyMyItem extends Component {

  constructor(props) {
    super(props)
    this.state = {
      containerOn: this.props.showModify,
      formData: {
        name: "",
        description: "",
        img: "",
        price: "",
        notif: false
      },
      messages: {
        messageStatus: ""
      },
      tests: {
        test: []
      }
    }
  }

  componentDidMount() {
    document.body.classList.add('noScrollBody')
  }

  componentWillUnmount() {
    document.body.classList.remove('noScrollBody')
  }

  handleChange = e => {
    const { name } = e.target
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    const { formData } = this.state
    this.setState({
      formData: {
        ...formData,
        [name]: value
      }
    })
  }

  handleFileUpload = e => {
    e.preventDefault()
    const { name } = e.target
    const { formData } = this.state
    let reader = new FileReader()
    let file = e.target.files[0]
    reader.onload = () => {
      this.setState({
        formData: {
          ...formData,
          [name]: reader.result
        }
      })
    }
    reader.readAsDataURL(file)
  }


  modifyMyItem = e => {
    e.preventDefault()

    axios.put(`${process.env.REACT_APP_URI}/api/modifymyitem/${this.props.itemId}`, this.state.formData, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item updated!") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  removeItem = () => {
    axios.put(`${process.env.REACT_APP_URI}/api/deleteitem`, { itemId: this.props.itemId }, {
      withCredentials: true
    })
      .then(data => {
        if (data.data == "Item deleted") {
          this.props.setStateMessages(data.data)
          this.props.closePopUpModify()
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    let { containerOn } = this.state

    return (
      <div className="popUpLog">
        {containerOn == 'modifyItem' && <div className="container-pop-up-modify-my-item">
          <div className="background-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}></div>
          <div className="pop-up-modify-my-item">
            <button className="close-pop-up-modify-my-item" onClick={() => this.props.closePopUpModify()}>X</button>
            <h2 className="text-align-center">Modify item</h2>
            <form className="post-form" ref={input => this.postFormItem = input} onSubmit={e => this.modifyMyItem(e)}>
              <div className="form-group">
                <input type="text" name="name" className="form-control" value={this.state.formData.name} placeholder="Name" onChange={this.handleChange} />
              </div>
              <div className="form-group">
                <input type="text" name="description" className="form-control" value={this.state.formData.description} placeholder="Description" onChange={this.handleChange} />
              </div>
              <div className="form-group ModifyItemPictureForm">
                <label>Item picture</label>
                <input type="file" name="img" className="form-control" placeholder="Item picture" onChange={this.handleFileUpload} />
              </div>
              <div className="input-group">
                <span className="input-group-addon">$</span>
                <input type="number" className="form-control" name="price" step="any" min="0" value={this.state.formData.price} placeholder="Price" aria-label="Amount (to the nearest dollar)" onChange={this.handleChange} />
              </div>
              <div className="form-check get-notif-item-sell">
                <input type="checkbox" name="notif" id="get-notified-by-email-post" className="intimy-checkbox" checked={this.state.formData.notif} onChange={this.handleChange} />
                <label htmlFor="get-notified-by-email-post"> Get notified by email</label>
              </div>
              <button className="btn btn-primary intimy-rectangle-button" type="submit">Submit</button>
              <button type="button" className="btn btn-danger float-right" onClick={() => this.removeItem()}>Delete</button>
            </form>
          </div>
        </div>}
      </div>
    )
  }
}

export default PopUpModifyMyItem;