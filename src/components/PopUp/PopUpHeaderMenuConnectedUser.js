//React
import React from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import onClickOutside from 'react-onclickoutside'

//Components
import ActionLink from '../ActionLink'

class PopUpHeaderMenuConnectedUser extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      containerOn: 'logIn'
    }
  }

  logOut = (e) => {
    e.preventDefault()
    const { cookies } = this.props
    cookies.remove('authtoken2', {path: '/'})
    window.location.reload()
  }

  handleFunctions = e => {
    this.props.menuConnectedUser()
  }

  render() {
    const {handleClickOutside} = this.props
    return (
      <div className="ContainerMenuConnectedUser">
        <ul className="ListItemsMenuConnectedUser">
          <li><ActionLink to="/myprofile" action={this.handleFunctions}>Account</ActionLink></li>
          <li><ActionLink to="/myfavorites" action={this.handleFunctions}>My favorites</ActionLink></li>
          <li><a href="#" onClick={this.logOut}>Log out</a></li>
        </ul>
      </div>
    )
  }
}

export default withCookies(onClickOutside(PopUpHeaderMenuConnectedUser))