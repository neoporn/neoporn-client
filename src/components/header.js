//React
import React from 'react'
//Router
import { BrowserRouter as Router, Route, Link, Redirect } from 'react-router-dom'
//Components
import HeaderNotConnected from './HeaderNotConnected'
import HeaderConnected from './HeaderConnected'
import userConnexionLoader from './UserConnectionLoader'
import axios from 'axios'
import PopUpLogIn from './PopUp/PopUpLogIn'

class Header extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      test: null,
      notConnectedMessage: false,
      showMobileMenu: null,
      showMobileSearch: null
    }
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    notConnectedMessage: false
  })

  showMobileMenu = e => {
    this.setState({
      showMobileMenu: !this.state.showMobileMenu,
      showMobileSearch: false
    })
  }

  showMobileSearch = e => {
    this.setState({
      showMobileSearch: !this.state.showMobileSearch,
      showMobileMenu: false
    })
  }

  render() {
    const { handleOutsideClick } = this.props
    return (
      <div>
        <header className="header">
          <div className="header-content">
            {!this.props.connected && <HeaderNotConnected handleOutsideClick={this.handleOutsideClick} outsideClickIgnoreClass={'toggle'} />}
            {this.props.connected && <HeaderConnected handleOutsideClick={this.handleOutsideClick} outsideClickIgnoreClass={'toggle'} />}
          </div>
        </header>
      </div>
    )
  }
}

export default userConnexionLoader(Header)