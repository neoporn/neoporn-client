//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import onClickOutside from 'react-onclickoutside'

//Components
import axios from 'axios'
import PopUpHeaderMenuConnectedUser from './PopUp/PopUpHeaderMenuConnectedUser'
import Notifications from './Notifications'
import UserConnexionLoader from './UserConnectionLoader'
import PopUpLogIn from './PopUp/PopUpLogIn'
import SearchBar from './SearchBar'
import SearchBarStore from "./stores/SearchBarStore"
import ActionLink from './ActionLink'
import { withCookies, Cookies } from 'react-cookie'

class HeaderConnected extends Component {

  constructor(props) {
    super(props)
    this.state = {
      menuConnectedUser: false,
      showMobileMenu: false,
      showMobileSearch: false,
      username: "",
      bagNumber: 0
    }
  }

  componentDidMount() {
    this.getUsername()
  }

  notConnectedMessage = e => {
    this.setState({
      notConnectedMessage: true
    })
    window.gtag('event', `upload button`, {
      'event_category': 'user',
      'event_label': `Not connected user`
    })
  }

  connectedUpload = e => {
    window.gtag('event', `upload button`, {
      'event_category': 'user',
      'event_label': `Connected user`
    })
  }

  home = e => {
    SearchBarStore.addString("")
  }

  getUsername() {
    axios.get(`${process.env.REACT_APP_URI}/api/getUsername`, { withCredentials: true })
      .then((response) => {
        if (response.data) {
          this.setState({
            username: response.data
          })
        }
        else {
          const { cookies } = this.props
          cookies.remove('authtoken2', { path: '/' })
          window.location.reload()
        }
      })
  }

  handleClickOutside = e => {
    this.handleOutsideClick()
  }

  handleOutsideClick = () => this.setState({
    menuConnectedUser: false
  })

  toggleMenuConnectedUser = (e) => {
    this.setState({
      menuConnectedUser: !this.state.menuConnectedUser,
      showMobileMenu: false
    })
  }

  showMobileMenu = e => {
    this.setState({
      showMobileMenu: !this.state.showMobileMenu,
      showMobileSearch: false
    })
  }

  showMobileSearch = e => {
    this.setState({
      showMobileSearch: !this.state.showMobileSearch,
      showMobileMenu: false
    })
  }

  logOut = (e) => {
    e.preventDefault()
    const { cookies } = this.props
    cookies.remove('authtoken2', { path: '/' })
    window.location.reload()
  }

  render() {
    const { handleOutsideClick } = this.props
    return (
      <div>
        <div className="flex align-center justify-space-between">
          <div onClick={this.showMobileMenu} className="mobile-menu-icon"><i className="fas fa-bars"></i></div>
          <div className="header-title"><a onClick={this.home} className="link-header" href="/">Neo<span className="porn">porn</span></a></div>
          <div onClick={this.showMobileSearch} className="mobile-search-icon"><i className="fas fa-search"></i></div>
          <div className="containerLogInHeader flex justify-flex-end align-center">
            <SearchBar />
            <span className="no-break-line-flex">
              <div className="logInHeader">
                <Link to="/upload" onClick={this.connectedUpload}><span className="upload-icon-container"><span className="upload-header"><i className="fas fa-cloud-upload-alt upload-icon"></i> Upload</span></span></Link>
                <Notifications />
                <span onClick={this.toggleMenuConnectionUser} className="username-header toggle">{this.state.username.username}<span onClick={this.toggleMenuConnectedUser}><i className="fas fa-chevron-down chevron-header"></i></span></span>
                {this.state.menuConnectedUser && <PopUpHeaderMenuConnectedUser handleClickOutside={this.handleClickOutside} outsideClickIgnoreClass={'toggle'} menuConnectedUser={this.toggleMenuConnectedUser} />}
              </div>
            </span>
          </div>
        </div>
        <div className="mobile-search">

          {this.state.showMobileSearch ?
            <div>
              <SearchBar toggleSearchBar={this.showMobileSearch} />
            </div>
            :
            this.state.showMobileMenu &&
            <div>
              <ul className="ListItemsMenuConnectedUser logged-link-header-container">
                <li onClick={this.showMobileMenu}><Link to="/myprofile">Account</Link></li>
                <li onClick={this.showMobileMenu}><Link to="/myfavorites">My favorites</Link></li>
                <li><a href="#" onClick={this.logOut}>Log out</a></li>
              </ul>
            </div>
          }
        </div>
      </div>
    )
  }
}

export default withCookies(UserConnexionLoader(onClickOutside(HeaderConnected)))