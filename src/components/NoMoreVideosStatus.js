import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class NoMoreVideosStatus extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            onMessage: false
        }
    }

    componentDidMount() {
        axios.put(`${process.env.REACT_APP_URI}/api/nomorevideosstatus`, { id: this.props.match.params.id })
            .then(data => {
                if (data.data === "Disabled") {
                    this.setState({
                        onMessage: true
                    })
                }
            })
    }

    render() {
        return (
            <div className="account-activated-container">
                {this.state.onMessage ?
                    <div className="account-activated-title">Emails notifications disabled</div>
                    :
                    <div className="account-activated-title">Emails notifications already disabled</div>
                }
            </div>
        )
    }
}

export default NoMoreVideosStatus