import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import VotingRecommendedVideos from './voting/VotingRecommendedVideos'

class RecommendedVideos extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            videos: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = e => {
        let { videos } = this.state
        axios.get(`${process.env.REACT_APP_URI}/api/getrecommendedvideos`)
            .then((response) => {
                if (response.data) {
                    this.setState({
                        videos: response.data,
                        loaded: true
                    })
                }
            })
    }

    render() {
        if (this.state.loaded) {
            const videos = this.state.videos
                .map((data) =>
                    <div className="recommended-videos-container" key={data._id}>
                        <a href={`/video/${data._id}`}>
                            <div className="recommended-video-content">
                                <img src={data.urlThumbnail} alt="thumbnail video" />
                                <p className="post-home-pseudo no-margin">{data.title}</p>
                                <div className="card-content-home flex justify-space-between">
                                    <span className="views-home">{data.views} Views</span>
                                    <VotingRecommendedVideos videoId={data._id}  likes={data.likes} dislikes={data.dislikes} />
                                </div>
                            </div>
                        </a>
                    </div>
                )
            return <div className="recommend-videos-justify">
                {videos}
            </div>
        }
        else return null
    }
}

export default RecommendedVideos