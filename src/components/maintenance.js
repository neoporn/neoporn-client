import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import axios from 'axios'
import _ from 'lodash'
import VotingHome from './voting/VotingHome'
import { observer } from "mobx-react"
import StoresHOC from "./stores/StoresHOC"

class CardHome extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      totalLength: null,
      cards: [],
      sortBy: null,
      searchString: null
    }
  }

  componentDidUpdate(prevProps, nextProps) {
    if (prevProps != this.props) {
      this.getData()
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = e => {
    let { cards } = this.state
    axios.get(`${process.env.REACT_APP_URI}/api/getHomeCards`, { params: { searchString: this.props.searchString } })
      .then((response) => {
        if (response.data.videos) {
          this.setState({
            cards: response.data.videos,
            totalLength: response.data.totalLength,
            loaded: true
          })
        }
      })
  }

  getSearchResult = e => {
    let { cards } = this.state
    axios.get(`${process.env.REACT_APP_URI}/api/getHomeCards`, { params: { skipSize: cards.length, searchString: this.props.searchString } })
      .then((response) => {
        if (response.data.videos) {
          this.setState({
            cards: [...cards, ...response.data.videos],
            totalLength: response.data.totalLength,
            loaded: true
          })
        }
      })
  }

  backResults = e => {
    this.props.searchForm([])
  }

  moreResults() {
    this.getSearchResult()
  }

  votes = e => {
    this.setState({
      likes: e.likes,
      dislikes: e.dislikes
    })
  }

  sortBy = e => {
    this.setState({
      sortBy: e
    })
  }

  getContestVideo = e => {
    axios.get(`${process.env.REACT_APP_URI}/api/getcontestvideo`)
      .then((response) => {
        if (response.data.video) {
          this.setState({
            sortBy: 'contestWinner',
            contestVideo: response.data.video,
            loaded: true
          })
        }
      })
  }

  render() {
    if (this.state.loaded) {
      return <div>
        <h1>Maintenance</h1>
        <h2>We will come back soon</h2>
        <h3>See you soon!</h3>
      </div>
    }
    else return null
  }
}

export default withRouter(StoresHOC(CardHome))