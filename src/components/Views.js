import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class Views extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loaded: false,
            totalLength: null,
            video: null
        }
        this.parent = React.createRef()
    }

    componentDidMount() {
        this.viewsFunction()
    }

    viewsFunction = () => {
        var video = this.props.children.ref.current
        let totalTimeViewed = 0
        var incTime

        video.addEventListener('play', () => {
            if (totalTimeViewed < 15) {
                incTime = setInterval(() => {
                    if (totalTimeViewed === 15) {
                        clearInterval(incTime)
                        axios.put(`${process.env.REACT_APP_URI}/api/incv`, { t: totalTimeViewed, videoId: this.props.videoId })
                        window.gtag('event', 'video seen', {
                            'event_category': 'user',
                            'event_label': `${this.props.videoName}`
                          })
                    }
                    else totalTimeViewed++
                }, 1000)
            }
        })
        video.addEventListener('pause', () => {
            clearInterval(incTime)
        })
    }

    render() {
        const {children} = this.props
        return (
            <Fragment>
                {children}
            </Fragment>
        )
    }
}

export default Views