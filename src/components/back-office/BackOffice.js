//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from '../UserConnectionLoader'
import VideosVerif from './VideosVerif'

class BackOffice extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: true,
            containerOn: null,
            message: ""
        }
    }

    setContainer = container => {
        this.setState({
            containerOn: container
        })
    }

    render() {
        if (!this.props.connected || this.props.role !== 3) {
            return <Redirect to="/" />
        }
        return (
            <div>
                <h2 className="text-center">Back office</h2>
                <div className="back-office-left-menu">
                    <span className="back-office-left-menu-item" onClick={() => this.setContainer("videosVerif")}>Videos verifications</span>
                </div>
                {this.state.containerOn === "videosVerif" && <VideosVerif />}
            </div>
        )
    }
}

export default UserConnexionLoader(BackOffice)