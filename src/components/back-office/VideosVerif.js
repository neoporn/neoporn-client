//React
import React, { Component } from 'react'
//Router
import { Router, Route, Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import UserConnexionLoader from '../UserConnectionLoader'

class VideosVerif extends Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            loaded: false,
            videoVerifData: [],
            message: "",
            verifForm: {
                reason: "",
                selectedOption: undefined
            }
        }
    }

    componentDidMount() {
        axios.get(`${process.env.REACT_APP_URI}/api/getvideosverif`)
            .then(data => {
                this.setState({
                    videoVerifData: data.data,
                    loaded: true
                })
            })
    }

    submit(data, e) {
        e.preventDefault()
        axios.put(`${process.env.REACT_APP_URI}/api/videoverif`, { videoId: data.videoId, validation: this.state.verifForm, userId: data.userId })
            .then(response => {
                let newVideosVerifData = this.state.videoVerifData
                let index = newVideosVerifData.findIndex(item => item._id === data.videoId)   
                newVideosVerifData.splice(index, 1)
                this.setState({
                    videoVerifData: newVideosVerifData
                })
            })
    }

    handleChange = e => {
        const { name, value } = e.target
        const { verifForm } = this.state
        this.setState({
            verifForm: {
                ...verifForm,
                [name]: value
            }
        })
    }

    render() {
        if (!this.props.connected) {
            return <Redirect to="/" />
        }
        if (this.state.loaded === true) {
            const videosUnverified = this.state.videoVerifData.map((video, index) =>
                <div key={index} className="id-request-user-container">
                    <ul className="id-request-user">
                        <li><video width="320" height="auto" src={video.url} controls preload="metadata"></video></li>
                    </ul>
                    <br />
                    <div>
                        <form className="verif-video-no" onSubmit={(e) => this.submit({ videoId: video._id, userId: video.uploader }, e)}>
                            <input type="radio" className="verif-video-yes" name="selectedOption" value="yes" onChange={this.handleChange} required />Yes
                            <br />
                            <input type="radio" className="verif-video-no" name="selectedOption" value="no" onChange={this.handleChange} required />No
                            <br />
                            <label htmlFor="reason">Reason</label>
                            <br />
                            <textarea name="reason" id="reason" className="textarea-reason" cols="30" rows="3" value={this.state.verifForm.reason} onChange={this.handleChange}></textarea>
                            <br />
                            <button type="submit">Ok</button>
                        </form>
                    </div>
                </div>
            )
            return (
                <div className="backoffice-container">
                    <h3 className="text-center">Videos verification</h3>
                    {videosUnverified}
                </div>
            )
        }
        else return null
    }
}

export default UserConnexionLoader(VideosVerif)