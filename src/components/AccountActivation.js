import React from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class AccountActivation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            onMessage: false
        }
    }

    componentDidMount() {
        document.title = "Account activation - neoporn.net"
        axios.put(`${process.env.REACT_APP_URI}/api/accountactivation`, { id: this.props.match.params.id })
            .then(data => {
                if (data.data === "Account activated") {
                    this.setState({
                        onMessage: true
                    })
                }
            })
    }

    render() {
        return (
            <div className="account-activated-container">
                {this.state.onMessage ?
                    <div className="account-activated-title">Your account was successfully activated</div>
                    :
                    <div className="account-activated-title">Account already activated</div>
                }
            </div>
        )
    }
}

export default AccountActivation