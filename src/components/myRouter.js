//React
import React, { Component } from 'react'
//Router
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom'
//Components
import Contact from './contact'
import MyProfile from './myProfile'
import MyFavorites from './MyFavorites'
import User from './User'
import Home from './home'
import Upload from './upload'
import Video from './video'
import NoMatch from './noMatch'
import NotificationsCenter from './NotificationsCenter'
import BackOffice from './back-office/BackOffice'
import AccountActivation from './AccountActivation'
import NoMoreVideosStatus from './NoMoreVideosStatus'
import ResetMyPassword from './ResetMyPassword'
import Maintenance from './maintenance'
import Tag from './Tag'
import Search from './Search'

class MyRouter extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/myprofile" component={MyProfile} />
          <Route exact path="/myfavorites" component={MyFavorites} />
          <Route exact path="/user/:id" component={User} />
          <Route exact path="/upload" component={Upload} />
          <Route exact path="/video/:videoid" component={Video} />
          <Route exact path="/backoffice" component={BackOffice} />
          <Route exact path="/activation/:id" component={AccountActivation} />
          <Route exact path="/nomorevideosstatus/:id" component={NoMoreVideosStatus} />
          <Route exact path="/resetmypassword/:id/:token" component={ResetMyPassword} />
          <Route exact path="/tag/:tag" component={Tag} />
          <Route exact path="/search/:query" component={Search} />
          <Route component={NoMatch} />
        </Switch>
      </div>
    )
  }
}

export default MyRouter