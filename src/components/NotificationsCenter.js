//React
import React, { Component } from 'react'
//Router
import { Redirect, Link } from 'react-router-dom'
//Components
import axios from 'axios'
import classNames from 'classnames'
import moment from 'moment'
import userConnexionLoader from './UserConnectionLoader'

class NotificationsCenter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      notifications: [],
      notificationsData: [],
      notificationNumber: 0,
      toggleDropdown: false,
      currentNotification: ""
    }
  }

  componentDidMount() {
    if (this.props.connected) this.getNotifications()
  }

  getNotifications() {
    axios.get(`${process.env.REACT_APP_URI}/api/getnotifications`, { withCredentials: true })
      .then(response => {
        const notificationsLenght = response.data.notifs.length
        const { buyers, produits, notifs, directSellProducts } = response.data
        const notificationsData = notifs.map(notification => {
          const buyer = buyers.find(buyer => notification.itemBoughtData.buyerId == buyer._id)
          let oldProduitsFound = produits.filter(produit => {
            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
          })
          let directSellProductsFound = directSellProducts.filter(produit => {
            return notification.itemBoughtData.itemId.indexOf(produit._id) != -1
          })
          let produitsFound = oldProduitsFound.concat(directSellProductsFound)
          return { ...notification, buyer, produitsFound }
        })
        this.setState({
          notifications: notifs,
          notificationNumber: notificationsLenght,
          notificationsData: notificationsData
        })
      })
  }

  notificationViewed() {
    if (this.state.notificationsData != "") {
      let data = this.state.notificationsData
      let index = data.findIndex(p => p._id == this.props.match.params.notifId)
      if (index == -1) {
        this.props.history.push('/')
      }
      else {
        if (data[index].seen == false) {
          data[index].seen = true
          axios.put(`${process.env.REACT_APP_URI}/api/notificationViewed`, { notifId: this.props.match.params.notifId }, { withCredentials: true })
        }
      }
    }
  }

  render() {
    if (!this.props.connected) return <Redirect to="/" />
    if (this.state.notificationsData != "") {
      { this.notificationViewed() }
      const currentNotification = this.state.notificationsData.find(items => items._id == this.props.match.params.notifId)
      const currentNotificationProduits = currentNotification.produitsFound.map((produit, index) =>
        <div key={index}>
          <img src="/images/logo.svg" alt="Profil picture buyer" className="image-current-notif" /> <p className="name-item-sold-notif-center">{produit.name} is sold</p>
          <br />
          <a className="btn-download-shipping-label" href={currentNotification.itemBoughtData.shippingLabelLink} role="button">Download shipping label</a>
        </div>
      )
      const notifications = this.state.notificationsData.map((items, index) => {
        const notificationStyle = classNames('notifications', {
          'notifications-background': items.seen == false,
        })
        return (
          <div key={index}>
            <Link to={`/notificationscenter/${items._id}`}>
              <div className={notificationStyle}>
                {items.produitsFound.map(produits => <span key={produits._id}><img src="/images/logo.svg" alt="Profil picture buyer" className="image-notif" /> {produits.name} </span>)}
                <span>is sold </span>
                <span className="notifs-time-ago">{moment(items.date).short()}</span>
              </div>
            </Link>
          </div>
        )
      }).reverse()
      return (
        <div className="row">
          <div className="container-notifs-center">
            {notifications}
          </div>
          {currentNotification &&
            <div className="container-notif-center">
              {currentNotificationProduits}
            </div>
          }
        </div>
      )
    }
    else {
      return null
    }
  }
}

export default userConnexionLoader(NotificationsCenter)