//React
import React, { Component } from 'react'
//Router
import { Link, Redirect, withRouter } from 'react-router-dom'
//Components
import axios from 'axios'
import { decorate, observable } from 'mobx'
import { observer } from 'mobx-react'
import SearchBarStore from "./stores/SearchBarStore"

class SearchBar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            searchString: ""
        }
    }

    searchSubmit(string, e) {
        e.preventDefault()
        SearchBarStore.addString(string)
        if (this.props.toggleSearchBar) this.props.toggleSearchBar()
        this.setState({
            searchString: ""
        })
        this.props.history.push(`/search/${string}`)
        window.gtag('event', `searchbar`, {
            'event_category': 'user',
            'event_label': string
        })
    }

    handleSearch = e => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <form className="search-form" onSubmit={(e) => this.searchSubmit(this.state.searchString, e)}>
                <input type="text" name="searchString" className="search-bar-header" placeholder="Search" onChange={this.handleSearch} value={this.state.searchString} />
            </form>
        )
    }
}

export default withRouter(observer(SearchBar))